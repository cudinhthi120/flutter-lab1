import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:manage_login/controllers/handle_logout.dart';
import 'package:manage_login/validate/validation.dart';
import 'package:manage_login/controllers/handle_signup.dart';
import '../controllers/handle_user.dart';
import 'login_page.dart';

class SignUpScreen extends StatefulWidget {
  static const route = '/signup';
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpState();
  }
}

class SignUpState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final usernameController = TextEditingController();
  final addressController = TextEditingController();
  final phonesController = TextEditingController();
  late String _emailAddress, _password, _username, _phone, _address, _confirmPassword;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Sign Up Page'),
        backgroundColor: Colors.indigo,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              topImage(),
              fieldUsername(),
              fieldAddress(),
              fieldPhone(),
              fieldEmailAddress(),
              fieldPassword(),
              fieldConfirmPassword(),
              signUpButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget topImage() {
    return Image.network(
      'https://buomxinh.vn/wp-content/uploads/2021/08/1628663137_620_Hinh-anh-cute-Avatar-chibi-sieu-de-thuong.jpg',
      width: MediaQuery.of(context).size.width * 0.3,
    );
  }

  Widget fieldUsername() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 16),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.name,
        controller: usernameController,
        cursorWidth: 2,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Full Name',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: validateSignUpName,
        onChanged: (value) {
          setState(() {
            _username = value.trim();
          });
        },
      ),
    );
  }

  Widget fieldPhone() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.phone,
        controller: phonesController,
        cursorWidth: 2,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Phone Number',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: validateSignUpPhone,
        onChanged: (value) {
          setState(() {
            _phone = value.trim();
          });
        },
      ),
    );
  }

  Widget fieldAddress() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.streetAddress,
        controller: addressController,
        cursorWidth: 2,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Address',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: validateSignUpAddress,
        onChanged: (value) {
          setState(() {
            _address = value.trim();
          });
        },
      ),
    );
  }

  Widget fieldEmailAddress() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: emailController,
        cursorWidth: 2,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Email',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: validateSignUpEmailAddress,
        onChanged: (value) {
          setState(() {
            _emailAddress = value.trim();
          });
        },
      ),
    );
  }

  Widget fieldPassword() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        controller: passwordController,
        cursorWidth: 2,
        obscureText: true,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Password',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: validateSignUpPassword,
        onChanged: (value) {
          setState(() {
            _password = value.trim();
          });
        },
      ),
    );
  }

  Widget fieldConfirmPassword() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 6),
      height: MediaQuery.of(context).size.height * 0.08,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        controller: confirmPasswordController,
        cursorWidth: 2,
        obscureText: true,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: 'Confirm password',
          hintStyle: TextStyle(fontSize: 15),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
        ),
        validator: (input) {
          if (input!.isEmpty) {
            return 'You are not confirm your password yet';
          }
          if (_confirmPassword != _password) {
            return 'Wrong password';
          }
          return null;
        },
        onChanged: (value) {
          setState(() {
            _confirmPassword = value.trim();
          });
        },
      ),
    );
  }

  Widget signUpButton() {
    return Container(
      margin: EdgeInsets.only(top: 16, bottom: 16, left: 16, right: 16),
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.055,
      decoration: BoxDecoration(
        color: Colors.green[700],
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextButton(
        onPressed: () async {
          try {
            if (formKey.currentState!.validate()) {
              formKey.currentState!.save();
              await HandleSignUp().signUp(
                email: _emailAddress,
                password: _password,
              );
              HandleUser().userInfo(
                username: _username,
                phone: _phone,
                address: _address,
                password: _password,
              );
              await HandleLogout().logOut();
              Navigator.of(context).pushReplacementNamed(LoginScreen.route);
            }
          } on FirebaseAuthException catch (e) {
            if (e.code == 'email-already-in-use') {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                backgroundColor: Colors.red,
                padding: EdgeInsets.all(20),
                content: Text("Sorry email already exists"),
              ));
            }
          }
        },
        child: Text(
          'Sign up'.toUpperCase(),
          style: TextStyle(
              color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }
}
