import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:manage_login/controllers/handle_logout.dart';
import 'package:manage_login/views/change_password_page.dart';
import 'package:manage_login/views/edit_user_info_page.dart';
import '../validate/validation.dart';
import 'login_page.dart';

class MainScreen extends StatefulWidget {
  static const route = '/main';

  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with CommonValidation {
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    final user = auth.currentUser?.email;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            if (user == null) {
              Navigator.of(context).pushReplacementNamed(LoginScreen.route);
            }
            if (user != null) {
              print('No way home');
            }
          },
          icon: Icon(Icons.arrow_back),
        ),
        title: Text(user == null ? "Hi" : "Hi ${user}"),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.indigo,
        foregroundColor: Colors.white,
      ),
      body: user == null ? BeforeLogin() : AfterLoginWithEmailAndPassword(),
    );
  }

  Widget BeforeLogin() {
    return Center(child: Text('Hi'));
  }

  Widget AfterLoginWithEmailAndPassword() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          editUserINfoButton(),
          changeUserPasswordButton(),
          LogoutButton(),
        ],
      ),
    );
  }

  Widget editUserINfoButton() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.of(context).pushNamed(EditUserInfoPage.route);
        },
        child: Text(
          'Edit info'.toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget changeUserPasswordButton() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.circular(6),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.of(context).pushNamed(ChangePasswordPage.route);
        },
        child: Text(
          'Change password'.toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget LogoutButton() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: 120,
      height: 38,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
        ),
        onPressed: () {
          if (auth.currentUser!.email != null) {
            HandleLogout().logOut();
          } else {
            print('No way home');
          }
        },
        child: Text('Log Out'.toUpperCase()),
      ),
    );
  }
}
