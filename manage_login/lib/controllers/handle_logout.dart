import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class HandleLogout {
  final auth = FirebaseAuth.instance;

  Future logOut () async{
    auth.signOut();
    print('Đăng xuất thành công');
  }
}
