import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:manage_login/views/change_password_page.dart';
import 'package:manage_login/views/edit_user_info_page.dart';
import 'package:manage_login/views/main_screen.dart';
import 'views/home_screen.dart';
import 'views/login_page.dart';
import 'views/sign_up_page.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: {
        '/home': (context) => HomeScreen(),
        '/main': (context) => MainScreen(),
        '/signup': (context) => SignUpScreen(),
        '/login': (context) => LoginScreen(),
        '/change-password': (context) => ChangePasswordPage(),
        '/edit-info': (context) => EditUserInfoPage(),
      },
      home: HomeScreen(),
    );
  }
}
