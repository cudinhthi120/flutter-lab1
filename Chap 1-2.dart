void main() {
  //variablePlayground();
  //stringPlayground();
  //functionPlayground();
  consumeClosure();
}

void variablePlayground(){
  // (optional modifier) (optional type) variableName = value;
  // var, final, const: Modifier
  // int, double, num, bool,.. : Type
  
  // Only specify a type when using final modifier. Not for var and const
  // Final variable cannot change, except internal contents of list, Object,...
  
  // Immutable > Mutable: faster, no side effect(unexpected cascade of changes)
  
  basicTypes();
  untypedVariables();
  typeInterpolation();
  immutableVariables();
}

void basicTypes() {
  int four = 4;
  double pi = 3.14;
  num someNumber = 24601;
  bool yes = true;
  bool no = false;
  int? nothing; // If null is a valid value, then make the variable nullable
  
  print(four);
  print(pi);
  print(someNumber);
  print(yes);
  print(no);
  print(nothing == null);
} 

void untypedVariables(){
  // Dynamic variable can be anything, 
  // Useful in some cases but should be avoided in most parts. 
  dynamic something = 14.2;
  print(something.runtimeType);
}

void typeInterpolation(){
  // Once a value has been assigned to variable, the type will be remembered
  // Cannot be changed later - Different from Dynamic variable.
  var anInteger = 15;
  var aDouble = 14.2;
  var aBoolean; // Null is assigned if don't supply a value

  print(anInteger.runtimeType);
  print(anInteger);
  
  print(aDouble.runtimeType);
  print(aDouble);
  
  print(aBoolean.runtimeType);
  print(aBoolean);
  
  aBoolean = true;
  print(aBoolean.runtimeType);
  print(aBoolean);
}

void immutableVariables() {
  // Different between final and const:
  // const variable MUST be determined at compile time
  // Ex: const cannot be used for DateTime.now() 
  //  since current Datetime can only be determined in runtime
  
  final int immutableInteger = 5;
  final double immutableDouble = 0.015;
  
  // Type annotation is optional
  final interpolatedInteger = 10;
  final interpolatedDouble = 72.8;
  print(interpolatedInteger);
  print(interpolatedDouble);
  
  const aFullySealedVariable = true;
  print(aFullySealedVariable);
}





void stringPlayground(){
  basicStringDeclaration();
  multiLineStrings();
  combiningStrings();
}

void basicStringDeclaration(){
  //With Single Quotes
  print('Single quotes');
  final aBoldStatement = 'Dart isn\'t loosely typed.';
  // With Double Quotes
  print("Hello, World");
  final aMoreMildOpinion = "Dart's popularity has skyrocketed with Flutter!";
  print(aMoreMildOpinion);
  // Combining single and double quotes
  final mixAndMatch = 'Every programmer should write "Hello, World" when learning a new language.';
   print(mixAndMatch);
}


void multiLineStrings() {
  final withEscaping = 'One Fish\nTwo Fish\nRed Fish\nBlue Fish';
  print(withEscaping);
  final hamlet = '''
To be, or not to be, that is the question:
Whether 'tis nobler in the mind to suffer
The slings and arrows of outrageous fortune,
Or to take arms against a sea of troubles
And by opposing end them.
    ''';
  print(hamlet);
}


void combiningStrings() {
  traditionalConcatenation();
  modernInterpolation();
  usingBuffer();
}

void traditionalConcatenation() {
  final hello = 'Hello';
  final world = "world";
  final combined = hello + ' ' + world;
  print(combined);
}

void modernInterpolation() {
  final year = 2011;
  final interpolated = 'Dart was announced in $year.';
  print(interpolated);
  final age = 35;
  final howOld = 'I am $age ${age == 1 ? 'year' : 'years'} old.';
  print(howOld);
}

void usingBuffer(){
  List fruits = ['Strawberry', 'Coconut', 'Orange', 'Mango', 'Apple'];
  StringBuffer buffer = StringBuffer();
  for (String fruit in fruits) {
    buffer.write(fruit);
    buffer.write(' ');
  }
  print (buffer.toString());
}



void functionPlayground(){
  optionalParameters();
}

void unnamed([String? name, int? age]){
  // ? after a parameter -> Nullable
  final actualName = name ?? 'Unknown';
  final actualAge = age ?? 0;
  print('$actualName is $actualAge ${actualAge == 1 ? 'year' : 'years'} old');
}

void named({String? greeting, String? name}) {
  // Need to specify parameter name, in any order.
  final actualGreeting = greeting ?? 'Hello';
  final actualName = name ?? 'Mystery Person';
  print('$actualGreeting, $actualName!');
}

String duplicate(String name, {int times = 1}) {
  // Default value instead of Null
  String merged = '';
  for (int i = 0; i < times; i++) {
    merged += name;
    if (i != times - 1) {
      merged += ' ';
    }
  }
  return merged;
}

void optionalParameters() {
  unnamed('Huxley',20);
  unnamed();
  // Notice how named parameters can be in any order
  named(greeting: 'Greetings and Salutations');
  named(name: 'Sonia');
  named(name: 'Alex',greeting: 'Bonjour');
  final multiply = duplicate('Mikey', times: 3);
  print(multiply);
}

void callbackExample(void callback(String value)){
  callback('Hello Callback');
}

typedef NumberGetter = int Function();

int powerOfTwo(NumberGetter getter){
  return getter() * getter();
}


void consumeClosure(){
  final getFour = () => 4;
  final squared = powerOfTwo(getFour);
  print(squared);
  
  callbackExample((result) {
    print(result);
  });
  
}
