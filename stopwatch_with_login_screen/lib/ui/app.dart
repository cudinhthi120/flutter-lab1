import 'package:flutter/material.dart';
import './login.dart';
import './stopwatch_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => LoginScreen(),
        '/login': (context) => LoginScreen(),
        StopWatchScreen.route: (context) => StopWatchScreen()
      },
      initialRoute: '/',
    );
  }

}
