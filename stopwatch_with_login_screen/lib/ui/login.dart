import 'package:flutter/material.dart';
import '../validation/validation.dart';
import 'stopwatch_screen.dart';

class LoginScreen extends StatefulWidget {
  static const route = '/login';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  late String emailAddress;
  late String password;

  @override
  Widget build(BuildContext context) {
    String backgroundImage = 'assets/background.png';
    return Scaffold(
      appBar: AppBar(title: Text('Login Page'),
      backgroundColor: Colors.orange[300],
        centerTitle: true,

      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 14),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(backgroundImage),
            fit: BoxFit.cover,
          )
        ),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldEmailAddress(),
              fieldPassword(),
              loginButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget fieldEmailAddress() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: emailController,
        decoration: const InputDecoration(
          icon: Icon(Icons.email_outlined),
          labelText: 'Email address',
          hintText: 'Email@gmail.com',
        ),
        validator: validateEmail,
      ),
    );
  }

  Widget fieldPassword() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20 ),
      child: TextFormField(
        obscureText: true,
        decoration: InputDecoration(
            icon: Icon(Icons.password),
            labelText: 'Password'
        ),
        validator: validatePassword,
      ),
    );
  }

  Widget loginButton() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      width: 100,
      height: 38,
      child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
          ),
          onPressed: validate,
          child: Text('Login'.toUpperCase())
      ),
    );
  }

  void validate() {
    final form = formKey.currentState;
    if (!form!.validate()) {
      return;
    } else {
      final email = emailController.text;

      setState(() {
        Navigator.of(context).pushReplacementNamed(StopWatchScreen.route, arguments: email);
      });
    }
  }
}

