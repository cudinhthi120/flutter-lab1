import 'package:cider_clone/services/firestore_db.dart';
import 'package:get/get.dart';
import '../models/new_in_products_model.dart';

class NewInProductController extends GetxController {
  final products = <NewInProduct>[].obs;

  @override
  void onInit(){
    products.bindStream(FirestoreDB().getAllNewInProducts());
    super.onInit();
  }
}
