import 'package:cider_clone/models/flash_sale_products_model.dart';
import 'package:cider_clone/services/firestore_db.dart';
import 'package:get/get.dart';

class FlashSaleProductController extends GetxController {
  final products = <FlashSaleProduct>[].obs;

  @override
  void onInit(){
    products.bindStream(FirestoreDB().getAllFlashSaleProducts());
    super.onInit();
  }
}