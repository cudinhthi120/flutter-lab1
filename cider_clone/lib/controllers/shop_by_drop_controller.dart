import 'package:cider_clone/models/shop_by_drop_items_model.dart';
import 'package:cider_clone/services/firestore_db.dart';
import 'package:get/get.dart';

class ShopByDropController extends GetxController{
  final listItems = <ShopByDropItems>[].obs;

  @override
  void onInit() {
    listItems.bindStream(FirestoreDB().getAllShopByDropItems());
    super.onInit();
  }
}