import 'package:cider_clone/models/inspiration_model.dart';
import 'package:cider_clone/services/firestore_db.dart';
import 'package:get/get.dart';

class InspirationController extends GetxController{
  final  listItems = <Inspiration>[].obs;

  @override
  void onInit() {
    listItems.bindStream(FirestoreDB().getAllInspirationItems());
    super.onInit();
  }
}