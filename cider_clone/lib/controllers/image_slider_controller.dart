import 'package:cider_clone/services/firestore_db.dart';
import 'package:get/get.dart';
import '../models/image_slider_model.dart';

class ImageSliderController extends GetxController {
  final imageList = <ImageSlider>[].obs;

  @override
  void onInit() {
    imageList.bindStream(FirestoreDB().getAllImageSlider());
    super.onInit();
  }
}