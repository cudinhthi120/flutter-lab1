import 'package:flutter/material.dart';

Widget borderButton(buttonTitle) {
  return Container(
    padding: EdgeInsets.only(bottom: 16, left: 16, right: 16, top: 8),
    width: double.infinity,
    child: TextButton(
      style: TextButton.styleFrom(
        textStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14
        ),
        primary: Colors.black,
        backgroundColor: Colors.amber[100],
        side: BorderSide(
          width: 2,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
      onPressed: () {},
      child: Text(buttonTitle),
    ),
  );
}
