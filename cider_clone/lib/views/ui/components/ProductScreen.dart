import 'package:cider_clone/views/ui/components/GirdViewWithListViewBelow.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:typicons_flutter/typicons_flutter.dart';
import '../components/BorderButton.dart';
import '../shopping_cart/ShoppingCart.dart';

class ProductScreen extends StatelessWidget {
  static const route = '/product';

  const ProductScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = ModalRoute.of(context)!.settings.arguments as String;
    double height = MediaQuery.of(context).size.height - 190;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1),
          child: Container(
            color: Colors.grey[300],
            height: 1,
          ),
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // Top Slider
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.amber[100],
                    ),
                    child: CarouselSlider.builder(
                      itemCount: 2,
                      itemBuilder: (context, index, pageViewIndex) => Center(
                        child: Text(
                          '20% off for your first App order! Code in bag!',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.pinkAccent),
                        ),
                      ),
                      options: CarouselOptions(
                        height: 40,
                        viewportFraction: 1,
                        autoPlayAnimationDuration: Duration(milliseconds: 300),
                        autoPlayInterval: Duration(seconds: 6),
                        initialPage: 0,
                        autoPlay: true,
                      ),
                    ),
                  ),
                  // Product Image
                  Container(
                    height: height,
                    color: Colors.orangeAccent,
                    child: Image.asset(
                      'assets/daily-flash-sale-listview-items-1.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  // Product info
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Product Name
                        Row(
                          children: [
                            Text(
                              'Floral Lace Patchy TankTop',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                            Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(right: 12),
                              child: Icon(
                                Typicons.heart_outline,
                                size: 28,
                              ),
                            ),
                            Icon(
                              Icons.file_upload,
                              size: 28,
                            ),
                          ],
                        ),
                        // Product Price
                        Row(
                          children: [
                            Text(
                              "US12.00",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'US20.00',
                                style: TextStyle(
                                  color: Colors.grey,
                                  decoration: TextDecoration.lineThrough,
                                  fontStyle: FontStyle.italic,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(4)),
                              child: Text(
                                '-40%',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 10),
                              ),
                            ),
                          ],
                        ),
                        // Product Color
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Row(
                            children: [
                              Text(
                                'Color',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: Text(
                                  'White',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 60,
                          width: 60,
                          margin: EdgeInsets.only(top: 16),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(
                              width: 2,
                              color: Colors.black,
                            ),
                          ),
                          child: Container(
                            margin: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.grey[350],
                                borderRadius: BorderRadius.circular(50)),
                          ),
                        ),
                        // Product Size
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Row(
                            children: [
                              Text(
                                'Size',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: Text(
                                  'XS',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(right: 4),
                                child: Icon(
                                  Icons.build_circle_rounded,
                                  size: 18,
                                  color: Colors.black,
                                ),
                              ),
                              Text(
                                'Sizing help',
                                style: TextStyle(
                                  color: Colors.black,
                                  decoration: TextDecoration.underline,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 75,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: 4,
                            itemBuilder: (context, index) => Container(
                              alignment: Alignment.center,
                              height: 60,
                              width: 60,
                              margin: EdgeInsets.only(top: 16, right: 16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                  width: 2,
                                  color: Colors.grey[300]!,
                                ),
                              ),
                              child: Text(
                                'XS',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        // Product Size Info
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          height: 81,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text('Out-of-stock'),
                              ),
                              Container(
                                height: 1,
                                color: Colors.grey[400],
                                margin: EdgeInsets.only(left: 12, right: 12),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                    'Bust:28.0inch/ 71cm, Waist:23.6inch / 60cm'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Shipping And Returns
                  Container(
                    padding: EdgeInsets.only(
                        left: 16, right: 16, bottom: 16, top: 32),
                    margin: EdgeInsets.only(top: 16),
                    width: double.infinity,
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Shipping + Returns',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Row(
                            children: [
                              dot(),
                              Text('Express shipping avaliable in'),
                              Padding(
                                padding: EdgeInsets.only(left: 4),
                                child: Text(
                                  'Andorra',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16),
                          height: 81,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                    'Estimated Shipping Time: 8-10 business days'),
                              ),
                              Container(
                                height: 1,
                                color: Colors.grey[400],
                                margin: EdgeInsets.only(left: 12, right: 12),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                    'Pre-order: not recommend using express shipping'),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16, bottom: 16),
                          child: Row(
                            children: [
                              dot(),
                              Text(
                                'International Shipping Policy',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            dot(),
                            Text(
                              'Return & Refund Policy',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  // Product Description
                  Container(
                    height: 80,
                    margin: EdgeInsets.only(top: 16),
                    padding: EdgeInsets.only(left: 16, right: 16),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Text(
                          'Product Description',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        Spacer(),
                        Icon(Typicons.plus),
                      ],
                    ),
                  ),
                  // Care Instruction
                  Container(
                    height: 80,
                    margin: EdgeInsets.only(top: 16),
                    padding: EdgeInsets.only(left: 16, right: 16),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Text(
                          'Care Instruction',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        Spacer(),
                        Icon(Typicons.plus),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16, bottom: 71),
                    color: Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 32, left: 16, bottom: 16),
                          child: Text(
                            'You May Also Like',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        // You May Also Like
                        GridViewWithListViewBelow()
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.white,
              child: borderButton('Add to Bag'.toUpperCase()),
            ),
          ),
        ],
      ),
    );
  }

  Widget dot() {
    return Container(
      height: 3,
      width: 2,
      color: Colors.black,
      margin: EdgeInsets.only(right: 12),
    );
  }
}
