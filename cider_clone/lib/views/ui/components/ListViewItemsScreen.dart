import 'GirdViewWithListViewBelow.dart';
import 'package:flutter/material.dart';
import '../search_screen/SearchScreen.dart';
import '../shopping_cart/ShoppingCart.dart';

class TopListViewItemsScreen extends StatelessWidget {
  static const route = '/top-listview';

  const TopListViewItemsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          title,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w900,
            wordSpacing: 2,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(SearchScreen.route);
              },
              child: Icon(
                Icons.search,
                size: 22,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(0.5),
          child: Container(
            color: Colors.grey[300],
            height: 0.5,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            _TopListView(),
            GridViewWithListViewBelow(),
          ],
        ),
      ),
    );
  }
}

class _TopListView extends StatelessWidget {
  const _TopListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 8),
            child: Icon(Icons.filter_list),
          ),
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.only(top: 8, bottom: 8),
              itemCount: 8,
              itemBuilder: (context, index) => Container(
                padding: EdgeInsets.only(left: 6, right: 6),
                margin: EdgeInsets.only(right: 4),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(30)
                ),
                child: TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    primary: Colors.black,
                  ),
                  child: Text('Retro', style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12
                  ),),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
