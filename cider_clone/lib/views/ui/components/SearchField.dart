import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  const SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 50,
              padding: const EdgeInsets.only(left: 16, top: 16),
              child: TextField(
                style: TextStyle(fontSize: 18, color: Colors.black),
                cursorWidth: 1.5,
                cursorHeight: 22,
                cursorColor: Colors.black,
                autofocus: true,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(
                    hintText: 'Looking for something?',
                    hintStyle: TextStyle(color: Colors.grey),
                    contentPadding: EdgeInsets.only(bottom: 18),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1)
                    ),
                    enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1)
                    )
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 8),
            child: Icon(Icons.search, color: Colors.black,),
          ),
        ],
      ),
    );
  }
}
