import 'package:flutter/material.dart';

class GridViewWithListViewBelow extends StatefulWidget {
  const GridViewWithListViewBelow({Key? key}) : super(key: key);

  @override
  _GridViewWithListViewBelowState createState() => _GridViewWithListViewBelowState();
}

class _GridViewWithListViewBelowState extends State<GridViewWithListViewBelow> {
  @override
  Widget build(BuildContext context) {
    List colors = [
      Colors.green,
      Colors.blue,
      Colors.grey,
      Colors.red,
      Colors.pink
    ];
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 220,
        mainAxisExtent: 360,
      ),
      padding: EdgeInsets.only(left: 10, right: 10),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 8,
      itemBuilder: (context, index) => Container(
        margin: EdgeInsets.all(6),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              child: Image.asset(
                'assets/our-top-picks-gridview-img.png',
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6, bottom: 6),
              child: Text(
                'Illusion Runched Midi Dress',
                style: TextStyle(fontSize: 12),
              ),
            ),
            Text(
              'USS28.00',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 8),
              height: 26,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 5,
                itemBuilder: (context, index) => Container(
                  width: 18,
                  margin: EdgeInsets.only(right: 16),
                  decoration: BoxDecoration(
                    color: colors[index],
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    ),
                  ),

                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
