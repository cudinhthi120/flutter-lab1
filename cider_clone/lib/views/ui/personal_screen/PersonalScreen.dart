import '../shopping_cart/ShoppingCart.dart';
import 'components/ProfileScreen.dart';
import 'package:flutter/material.dart';
import '../../../demo_data.dart';
import '../login_signup/LoginMethod.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PersonalScreen extends StatefulWidget {
  static const route = '/personal';
  const PersonalScreen({Key? key}) : super(key: key);

  @override
  _PersonalScreenState createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.red[400],
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.grey[100],
      body: SingleChildScrollView(
        child: Column(
          children: [
            TopImage(),
            Profile(),
            FirstListView(),
            SecondListView(),
          ],
        ),
      ),
    );
  }
}

class TopImage extends StatelessWidget {
  const TopImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      width: double.infinity,
      color: Colors.red[400],
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
                height: 90,
                child: Image.asset(
                  'assets/personal-login-img.png',
                  fit: BoxFit.cover,
                )),
          ),
          GestureDetector(
            onTap: () {
              showModalBottomSheet<void>(
                backgroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(16),
                  ),
                ),
                isScrollControlled: true,
                context: context,
                builder: (BuildContext context) {
                  return LoginMethod();
                },
              );
            },
            child: Text(
              'login'.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(ProfileScreen.route);
      },
      child: Padding(
        padding: const EdgeInsets.only(
          top: 16,
          bottom: 16,
        ),
        child: Items(
          leadingIcon,
          title,
          subTitle,
          trailingIcon
        ),
      ),
    );
  }
}

class FirstListView extends StatefulWidget {
  const FirstListView({Key? key}) : super(key: key);

  @override
  _FirstListViewState createState() => _FirstListViewState();
}

class _FirstListViewState extends State<FirstListView> {


  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: firstListViewItems.length,
      itemBuilder: (context, index) => GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(firstListViewItems[index]['route']);
        },
        child: Items(
          firstListViewItems[index]['leadingIcon'],
          firstListViewItems[index]['title'],
          firstListViewItems[index]['subTitle'],
          firstListViewItems[index]['trailingIcon'],
        ),
      ),
    );
  }
}

class SecondListView extends StatefulWidget {
  const SecondListView({Key? key}) : super(key: key);

  @override
  _SecondListViewState createState() => _SecondListViewState();
}

class _SecondListViewState extends State<SecondListView> {


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 4,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(secondLisViewItems[index]['route']);
          },
          child: Items(
            secondLisViewItems[index]['leadingIcon'],
            secondLisViewItems[index]['title'],
            secondLisViewItems[index]['subTitle'],
            secondLisViewItems[index]['trailingIcon'],
          ),
        ),
      ),
    );
  }
}

Widget Items(leadingIcon, title, subTitle, trailingIcon) {
  return Container(
    color: Colors.white,
    padding: EdgeInsets.all(12),
    child: Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Icon(leadingIcon, size: 18,),
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
          ),
        ),
        Spacer(),
        Text(
          subTitle,
          style: TextStyle(fontSize: 12, color: Colors.grey),
        ),
        Icon(
          trailingIcon,
          size: 28,
        )
      ],
    ),
  );
}
