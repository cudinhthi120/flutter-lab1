import 'package:cider_clone/demo_data.dart';
import 'package:cider_clone/views/ui/components/ListViewAppBar.dart';
import 'package:flutter/material.dart';

class CustomerService extends StatelessWidget {
  static const route = '/customer-service';

  const CustomerService({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar(''),
      backgroundColor: Colors.grey[200],
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TopTitle(),
            SearchFieldAndCommon(),
            BodyListView(),
            ServiceIntroduction(),
          ],
        ),
      ),
    );
  }
}

class TopTitle extends StatelessWidget {
  const TopTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.only(top: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/customer-service-top-title-image.png'),
              Text(
                'Cider Customer Service',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
              ),
              Image.asset('assets/customer-service-top-title-image.png'),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Find common answers here',
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SearchFieldAndCommon extends StatelessWidget {
  const SearchFieldAndCommon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(
            width: double.infinity,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    height: 50,
                    padding: const EdgeInsets.only(left: 16, top: 16),
                    child: TextField(
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      cursorWidth: 1.5,
                      cursorHeight: 22,
                      cursorColor: Colors.black,
                      autofocus: true,
                      textAlignVertical: TextAlignVertical.center,
                      decoration: InputDecoration(
                          hintText: 'Search your question',
                          hintStyle: TextStyle(color: Colors.grey),
                          contentPadding: EdgeInsets.only(bottom: 18),
                          focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1)),
                          enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 1))),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 16, left: 8, right: 16, bottom: 8),
                  child: Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BodyListView extends StatefulWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override
  State<BodyListView> createState() => _BodyListViewState();
}

class _BodyListViewState extends State<BodyListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      color: Colors.white,
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: customerServices.length,
        itemBuilder: (context, index) => Container(
          margin: EdgeInsets.only(left: 16),
          padding: EdgeInsets.only(top: 16, bottom: 16, right: 8),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(width: 1.5, color: Colors.grey[200]!))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(customerServices[index]['icon']),
              ),
              Text(
                customerServices[index]['title'],
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w900),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ServiceIntroduction extends StatelessWidget {
  const ServiceIntroduction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 32, bottom: 8),
            child: Text(
              'Service Introduction',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w900),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 32),
            child: Text(
              'More question can be asked here!',
              style: TextStyle(color: Colors.grey, fontSize: 14),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width *0.95,
            height: 120,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 3,
              itemBuilder: (context, index) => Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius: BorderRadius.circular(6)),
                margin: EdgeInsets.only(left: 8, right: 8),
                height: 110,
                width: 115,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 30,
                      margin: const EdgeInsets.only(top: 12, bottom: 8),
                      child: Image.asset(
                        serviceIntroduction[index]['icon'],
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      serviceIntroduction[index]['title'],
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 15,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 6, bottom: 6, left: 16, right: 16),
                      child: Text(
                        serviceIntroduction[index]['subTitle'],
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 24, bottom: 24, left: 16, right: 16),
            color: Colors.grey,
            height: 0.5,
          ),
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 32),
            child: Text(
              'Need more support? Please feel free to reach us directly at hi@shopcider.com',
              style: TextStyle(color: Colors.grey, fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }
}
