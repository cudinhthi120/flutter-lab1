import 'package:cider_clone/views/ui/components/BorderButton.dart';
import 'package:cider_clone/views/ui/components/ListViewAppBar.dart';
import 'package:flutter/material.dart';

class OrderScreen extends StatelessWidget {
  static const route = '/orders';

  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('My Orders'),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.only(top: 36),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8, top: 16),
              child: Image.asset(
                'assets/wishlist-top-img.png',
                fit: BoxFit.cover,
              ),
            ),
            Text(
              'You have no order yet',
              style: TextStyle(color: Colors.grey),
            ),
            borderButton('Go Order'.toUpperCase()),
          ],
        ),
      ),
    );
  }
}
