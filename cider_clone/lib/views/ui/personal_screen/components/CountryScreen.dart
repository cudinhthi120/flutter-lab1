import 'package:flutter/material.dart';
import '../../components/ListViewAppBar.dart';
import '../../components/SearchField.dart';

class CountryScreen extends StatelessWidget {
  static const route ='/country';
  const CountryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('Country/Region'),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          SearchField()
        ],
      ),
    );
  }
}
