import 'package:cider_clone/demo_data.dart';
import '../../components/ListViewAppBar.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  static const route = '/profile';

  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('Profile'),
      backgroundColor: Colors.grey[100],
      body: Container(
        margin: EdgeInsets.only(top: 16),
        color: Colors.white,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: 4,
          itemBuilder: (context, index) => Container(
            margin: EdgeInsets.only(left: 16),
            padding: EdgeInsets.only(top: 20, bottom: 20, right: 8),
            decoration: BoxDecoration(
                color: Colors.white,
                border:
                    Border(bottom: BorderSide(width: 1.5, color: Colors.grey[200]!))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  profileListViewItems[index]['title'],
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey[850]),
                ),
                Spacer(),
                Text(
                  profileListViewItems[index]['subTitle'],
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    size: 16,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
