import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../components/GirdViewWithListViewBelow.dart';

class YouMayAlsoLike extends StatefulWidget {
  const YouMayAlsoLike({Key? key}) : super(key: key);

  @override
  _YouMayAlsoLikeState createState() => _YouMayAlsoLikeState();
}

class _YouMayAlsoLikeState extends State<YouMayAlsoLike> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        YouMayAlsoLikeTitle(),
        YouMayAlsoLikeContent(),
      ],
    );
  }
}

class YouMayAlsoLikeTitle extends StatelessWidget {
  const YouMayAlsoLikeTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Text(
        'You May Also Like',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 14,
        ),
      ),
    );
  }
}

class YouMayAlsoLikeContent extends StatefulWidget {
  const YouMayAlsoLikeContent({Key? key}) : super(key: key);

  @override
  _YouMayAlsoLikeContentState createState() => _YouMayAlsoLikeContentState();
}

class _YouMayAlsoLikeContentState extends State<YouMayAlsoLikeContent> {
  @override
  Widget build(BuildContext context) {
    return GridViewWithListViewBelow();
  }
}
