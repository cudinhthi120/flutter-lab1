import 'package:cider_clone/views/ui/components/ListViewItemsScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TopListView extends StatefulWidget {
  const TopListView({Key? key}) : super(key: key);

  @override
  _TopListViewState createState() => _TopListViewState();
}

class _TopListViewState extends State<TopListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 90,
     child: StreamBuilder(
       stream: FirebaseFirestore.instance.collection('TopListViewItems').snapshots(),
       builder: (context, AsyncSnapshot snapshot) {
         if(!snapshot.hasData) return Text("Loading...");
         return ListView.builder(
             padding: EdgeInsets.only(left: 12, right: 12),
             scrollDirection: Axis.horizontal,
             itemCount: snapshot.data.docs.length,
             itemBuilder: (context, index) => GestureDetector(
               onTap: () {
                 Navigator.of(context).pushNamed(TopListViewItemsScreen.route,
                     arguments: snapshot.data.docs[index]['title']);
               },
               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,
                 children: [
                   Container(
                     alignment: Alignment.center,
                     margin: EdgeInsets.only(left: 8, right: 8, top: 12, bottom: 4),
                     child: CircleAvatar(
                       backgroundImage: NetworkImage(snapshot.data.docs[index]['image']),
                     ),
                   ),
                   Container(
                     width: 45,
                     margin: EdgeInsets.only(left: 8, right: 8),
                     child: Text(
                       snapshot.data.docs[index]['title'],
                       maxLines: 1,
                       textAlign: TextAlign.center,
                       overflow: TextOverflow.ellipsis,
                       style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),
                     ),
                   ),
                 ],
               ),
             ),
         );
       },
     ),
    );
  }
}
