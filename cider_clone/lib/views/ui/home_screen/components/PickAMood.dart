import 'package:cider_clone/demo_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../components/ListViewItemsScreen.dart';

class PickAMood extends StatefulWidget {
  const PickAMood({Key? key}) : super(key: key);

  @override
  _PickAMoodState createState() => _PickAMoodState();
}

class _PickAMoodState extends State<PickAMood> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PickAMoodTitle(),
        PickAMoodGridView(),
      ],
    );
  }
}

class PickAMoodTitle extends StatelessWidget {
  const PickAMoodTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Text(
        'Pick A Mood',
        style: TextStyle(
          fontSize: 26,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class PickAMoodGridView extends StatefulWidget {
  const PickAMoodGridView({Key? key}) : super(key: key);

  @override
  _PickAMoodGridViewState createState() => _PickAMoodGridViewState();
}

class _PickAMoodGridViewState extends State<PickAMoodGridView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 450,
      child: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('PickAMoodItems').snapshots(),
        builder: (context, AsyncSnapshot snapshot) {
          if(!snapshot.hasData) return Text('Loading...');
          return GridView.builder(
            padding: EdgeInsets.only(left: 10, right: 10),
            physics: NeverScrollableScrollPhysics(),
            itemCount: snapshot.data.docs.length,
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 190,
            ),
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(TopListViewItemsScreen.route, arguments: snapshot.data.docs[index]['title']);
              },
              child: Container(
                margin: EdgeInsets.all(6),
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: Image.asset(
                        snapshot.data.docs[index]['image'],
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        color: pickAMood[index],
                        child: Text(
                          snapshot.data.docs[index]['title'],
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      )
    );
  }
}
