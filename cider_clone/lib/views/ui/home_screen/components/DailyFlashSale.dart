import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../controllers/flash_sale_controller.dart';
import '../../components/ProductScreen.dart';

class DailyFlashSale extends StatefulWidget {
  const DailyFlashSale({Key? key}) : super(key: key);

  @override
  _DailyFlashSaleState createState() => _DailyFlashSaleState();
}

class _DailyFlashSaleState extends State<DailyFlashSale> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DailyFlashSaleTitle(),
        DailyFlashSaleListView(),
      ],
    );
  }
}

class DailyFlashSaleTitle extends StatelessWidget {
  const DailyFlashSaleTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Text(
            'Daily Flash Sale',
            style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
          ),
          Image.asset('assets/daily-flash-sale-title-img.png'),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.remove_red_eye_outlined,
              color: Colors.black54,
            ),
          ),
          Text(
            'See all',
            style: TextStyle(color: Colors.black54),
          ),
        ],
      ),
    );
  }
}

class DailyFlashSaleListView extends StatelessWidget {
  final productController = Get.put(FlashSaleProductController());
  DailyFlashSaleListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        height: 250,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.only(left: 8, right: 16),
          itemCount: productController.products.length,
          itemBuilder: (context, index) {
            return _buildListItems(index: index);
          },
        ),
      ),
    );
  }
}

class _buildListItems extends StatelessWidget {
  final int index;
  final FlashSaleProductController productController = Get.find();

  _buildListItems({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(ProductScreen.route,
            arguments: productController.products[index].name);
      },
      child: Container(
        width: 110,
        margin: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
                height: 150,
                child: Image.asset(
                  productController.products[index].image,
                  fit: BoxFit.cover,
                )),
            Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Text(
                productController.products[index].name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 12),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 4, bottom: 4),
              child: Text(
                productController.products[index].newPrice,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.deepOrangeAccent,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ),
            Text(
              productController.products[index].oldPrice,
              style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 12,
                  fontStyle: FontStyle.italic,
                  decoration: TextDecoration.lineThrough),
            ),
          ],
        ),
      ),
    );
  }
}
