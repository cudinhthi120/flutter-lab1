import 'package:cider_clone/controllers/inspiration_controller.dart';
import 'package:cider_clone/views/ui/components/ListViewItemsScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Inspiration extends StatefulWidget {
  const Inspiration({Key? key}) : super(key: key);

  @override
  _InspirationState createState() => _InspirationState();
}

class _InspirationState extends State<Inspiration> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InspirationTitle(),
        InspirationContent(),
      ],
    );
  }
}

class InspirationTitle extends StatelessWidget {
  const InspirationTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Inspiration',
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.bold,
            ),
          ),
          Image.asset('assets/inspiration-title-img.png'),
        ],
      ),
    );
  }
}

class InspirationContent extends StatelessWidget {
  final itemsController = Get.put(InspirationController());

  InspirationContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: 700,
        padding: const EdgeInsets.all(16.0),
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: itemsController.listItems.length,
            itemBuilder: (context, index) {
              return _buildListItems(
                index: index,
              );
            }),
      ),
    );
  }
}

class _buildListItems extends StatelessWidget {
  final int index;
  final InspirationController itemsController = Get.find();

  _buildListItems({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(
          TopListViewItemsScreen.route,
          arguments: itemsController.listItems[index].title,
        );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 50.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: double.infinity,
              child: Image.asset(itemsController.listItems[index].image,
                  fit: BoxFit.fill),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              child: Text(
                itemsController.listItems[index].title,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              itemsController.listItems[index].content,
              style: TextStyle(
                fontSize: 15,
                color: Colors.grey[600],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
