import 'package:cider_clone/controllers/new_in_controller.dart';
import 'package:get/get.dart';
import '../../components/ProductScreen.dart';
import 'package:flutter/material.dart';

class NewIn extends StatelessWidget {
  const NewIn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NewInTitle(),
        NewInImageListView(),
      ],
    );
  }
}

class NewInTitle extends StatelessWidget {
  const NewInTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'New In',
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Image.asset('assets/new-in-title-img.png'),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.remove_red_eye_outlined,
                  color: Colors.black54,
                ),
              ),
              Text(
                'See All',
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ],
          ),
          Text(
            'Fun Fresh Must Have',
            style: TextStyle(
              fontSize: 12,
              color: Colors.black54,
            ),
          ),
        ],
      ),
    );
  }
}

class NewInImageListView extends StatelessWidget {
  final productController = Get.put(NewInProductController());

  NewInImageListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SizedBox(
        height: 250,
        child: ListView.builder(
          padding: EdgeInsets.only(left: 8, right: 16),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: productController.products.length,
          itemBuilder: (context, index) {
            return _buildListItems(index: index);
          },
        ),
      ),
    );
  }
}

class _buildListItems extends StatelessWidget {
  final int index;
  final NewInProductController productController = Get.find();

  _buildListItems({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(ProductScreen.route,
            arguments: productController.products[index].name);
      },
      child: Container(
        width: 110,
        margin: EdgeInsets.only(left: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 150,
              child: Image.asset(
                productController.products[index].image,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6.0),
              child: Text(
                productController.products[index].name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 12),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 4, bottom: 4),
              child: Text(
                productController.products[index].price,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ),
            Text(
              productController.products[index].colors,
              style: TextStyle(
                fontSize: 12,
                color: Colors.grey[600],
              ),
            )
          ],
        ),
      ),
    );
  }
}
