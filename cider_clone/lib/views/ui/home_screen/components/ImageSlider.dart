import 'package:cider_clone/controllers/image_slider_controller.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:get/get.dart';

class ImageSlider extends StatelessWidget {
  final imageSliderController = Get.put(ImageSliderController());

  ImageSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => CarouselSlider.builder(
        itemCount: imageSliderController.imageList.length,
        options: CarouselOptions(
          height: 550,
          autoPlay: true,
          initialPage: 0,
          viewportFraction: 1,
          autoPlayAnimationDuration: Duration(milliseconds: 500),
        ),
        itemBuilder: (context, index, pageViewIndex) {
          return _buildImageList(index: index);
        },
      ),
    );
  }
}

class _buildImageList extends StatelessWidget {
  final int index;
  final ImageSliderController imageSliderController = Get.find();

  _buildImageList({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      imageSliderController.imageList[index].image,
      fit: BoxFit.cover,
    );
  }
}
