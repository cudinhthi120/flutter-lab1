import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class TopSlider extends StatelessWidget {
  const TopSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.amber[100],
      ),
      child: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('TopSliderItems').snapshots(),
        builder: (context, AsyncSnapshot snapshot) {
          if(!snapshot.hasData) return Text('Loading...');
          return CarouselSlider.builder(
            itemCount: 2,
            itemBuilder: (context, index, pageViewIndex) => Center(
              child: Text(
                snapshot.data.docs[index]['content'],
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.pinkAccent),
              ),
            ),
            options: CarouselOptions(
              height: 40,
              viewportFraction: 1,
              autoPlayAnimationDuration: Duration(milliseconds: 300),
              autoPlayInterval: Duration(seconds: 6),
              initialPage: 0,
              autoPlay: true,
            ),
          );
        },
      ),
    );
  }
}
