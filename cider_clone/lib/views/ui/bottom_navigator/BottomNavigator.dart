import 'package:flutter/material.dart';
import 'package:typicons_flutter/typicons_flutter.dart';
import '../category_screen/CategoryScreen.dart';
import '../home_screen/HomeScreen.dart';
import '../personal_screen/PersonalScreen.dart';
import '../wishlist_screen/WishListScreen.dart';

class BottomNavigator extends StatefulWidget {
  static const route = '/';
  const BottomNavigator({Key? key}) : super(key: key);

  @override
  _BottomNavigatorState createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator> {
  int currentIndex = 0;

  final List screens = [
    const HomeScreen(),
    const CategoryScreen(),
    const WishListScreen(),
    const PersonalScreen(),
  ];

  final List items = [
    {
      'label': 'Home',
      'icon': Typicons.home_outline,
    },
    {
      'label': 'Category',
      'icon': Icons.search,
    },
    {
      'label': 'Whislist',
      'icon': Typicons.heart_outline,
    },
    {
      'label': 'Me',
      'icon': Icons.emoji_emotions_outlined,
    },
  ];

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTap,
        elevation: 0,
        currentIndex: currentIndex,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedLabelStyle: TextStyle(
          fontSize: 10,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 10,
        ),
        selectedIconTheme: IconThemeData(
          size: 28,
        ),
        unselectedIconTheme: IconThemeData(
          size: 28,
        ),
        unselectedItemColor: Colors.grey[400],
        selectedItemColor: Colors.black,
        items: items
            .map((item) => BottomNavigationBarItem(
                icon: Icon(item['icon']), label: item['label']))
            .toList(),
      ),
    );
  }
}


