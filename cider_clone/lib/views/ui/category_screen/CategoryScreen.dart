import 'package:cider_clone/demo_data.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_custom_tab_bar/library.dart';
import '../shopping_cart/ShoppingCart.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          foregroundColor: Colors.black,
          backgroundColor: Colors.white,
          title: Text(
            'Cinder'.toUpperCase(),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(ShoppingCart.route);
                },
                child: Icon(
                  Icons.shopping_bag_outlined,
                  size: 22,
                ),
              ),
            )
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(40),
            child: _SearchField(),
          ),
        ),
        body: Column(
          children: [
            TopSlider(),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: SideBar(),
              ),
            ),
          ],
        ));
  }
}

class _SearchField extends StatelessWidget {
  const _SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 8, left: 16, right: 16),
      height: 45,
      color: Colors.white,
      child: TextField(
        cursorWidth: 1,
        cursorColor: Colors.black,
        maxLines: 1,
        textAlignVertical: TextAlignVertical.center,
        autofocus: false,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey[200],
          prefixIcon: Icon(
            Icons.search,
            color: Colors.grey,
          ),
          contentPadding: EdgeInsets.zero,
          hintText: 'Search',
          hintStyle: TextStyle(
              fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(18),
          ),
        ),
      ),
    );
  }
}

class TopSlider extends StatelessWidget {
  const TopSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.amber[50],
          border:
              Border(top: BorderSide(color: Colors.grey[400]!, width: 0.5))),
      child: CarouselSlider.builder(
        itemCount: 2,
        itemBuilder: (context, index, pageViewIndex) => Center(
          child: Text(
            'Get 15% Off for 1st Order! Use Code: CIDER',
            style: TextStyle(fontSize: 14, color: Colors.pinkAccent),
          ),
        ),
        options: CarouselOptions(
          height: 40,
          viewportFraction: 1,
          autoPlayAnimationDuration: Duration(milliseconds: 300),
          autoPlayInterval: Duration(seconds: 6),
          initialPage: 0,
          autoPlay: true,
        ),
      ),
    );
  }
}

class SideBar extends StatefulWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  final int pageCount = sideBarItem.length;
  late PageController _controller =
      PageController(initialPage: 0, viewportFraction: 1);
  CustomTabBarController _tabBarController = CustomTabBarController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CustomTabBar(
          tabBarController: _tabBarController,
          width: 130,
          direction: Axis.vertical,
          itemCount: pageCount,
          builder: (context, index) => TabBarItem(
            index: index,
            transform: ColorsTransform(
                highlightColor: Colors.black,
                normalColor: Colors.grey,
                builder: (context, color) {
                  return Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 12, bottom: 12, left: 8),
                    child: Text(
                      sideBarItem[index],
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: color,
                      ),
                    ),
                  );
                }),
          ),
          indicator: RoundIndicator(
            top: 10,
            bottom: 10,
            right: 125,
            color: Colors.black,
          ),
          pageController: _controller,
        ),
        Expanded(
          child: PageView.builder(
            scrollDirection: Axis.vertical,
            controller: _controller,
            itemCount: pageCount,
            itemBuilder: (context, pageNumber) {
              return PageItems(pageNumber);
            },
          ),
        )
      ],
    );
  }
}

class PageItems extends StatelessWidget {
  final int pageNumber;

  const PageItems(this.pageNumber, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 16, left: 16, top: 8),
          child: Text(
            sideBarItem[pageNumber],
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(
          child: GridView.builder(
            padding: EdgeInsets.only(left: 10, right: 10),
            physics: NeverScrollableScrollPhysics(),
            itemCount: 2,
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 160,
            ),
            itemBuilder: (context, index) => Container(
              margin: EdgeInsets.only(left: 4, bottom: 8, right: 4),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      width: double.infinity,
                      height: double.infinity,
                      child: Image.asset(
                        pageItems[pageNumber]['items'][index]['image'],
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      alignment: Alignment.center,
                      height: 35,
                      width: double.infinity,
                      color: Colors.grey[100],
                      child: Text(
                        pageItems[pageNumber]['items'][index]['name'],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
