import 'package:provider/provider.dart';
import '../../../demo_data.dart';
import '../../../services/google_sign_in.dart';
import 'SignUpAndLogin.dart';
import 'package:flutter/material.dart';

class LoginMethod extends StatefulWidget {
  const LoginMethod({Key? key}) : super(key: key);

  @override
  State<LoginMethod> createState() => _LoginMethodState();
}

class _LoginMethodState extends State<LoginMethod> {
  int currentView = 0;
  List? pages;

  @override
  void initState() {
    pages = [
      OtherLoginMethod(),
      SignUpAndLogin(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height *0.95,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Icon(Icons.highlight_off_outlined),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Image.asset(
              'assets/login-title-img.png',
              fit: BoxFit.fill,
            ),
          ),
          pages?[currentView]
        ],
      ),
    );
  }

  Widget OtherLoginMethod() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 3,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.only(bottom: 4, left: 32, right: 32),
            child: TextButton(
              onPressed: () {
                final provider =
                    Provider.of<GoogleSignInProvider>(context, listen: false);
                provider.googleLogin();
              },
              style: TextButton.styleFrom(
                  primary: Colors.black,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(width: 1, color: Colors.grey[300]!))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Image.asset(
                      loginMethodContent[index]['icon'],
                      fit: BoxFit.cover,
                    ),
                  ),
                  Text(loginMethodContent[index]['text']),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(28),
          width: double.infinity,
          child: Image.asset(
            'assets/exclusive-img.png',
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Text(
            'No account?',
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              currentView = 1;
            });
          },
          child: Text(
            'Create one',
            style: TextStyle(
              color: Colors.red,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
