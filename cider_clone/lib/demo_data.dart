import 'package:flutter/material.dart';

// Personal Screen
IconData leadingIcon = Icons.account_circle_outlined;
var title = 'My Profile';
var subTitle = '';
IconData trailingIcon = Icons.keyboard_arrow_right;

List firstListViewItems = [
  {
    'leadingIcon': Icons.calendar_today_outlined,
    'title': 'Order',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/orders',
  },
  {
    'leadingIcon': Icons.location_on_outlined,
    'title': 'Address Book',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/address-book',
  },
  {
    'leadingIcon': Icons.headset_outlined,
    'title': 'Customer Service',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/customer-service',
  },
];

List secondLisViewItems = [
  {
    'leadingIcon': Icons.language_outlined,
    'title': 'Country/Region',
    'subTitle': 'United States',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/country'
  },
  {
    'leadingIcon': Icons.chat_outlined,
    'title': 'Language',
    'subTitle': 'English',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/language'
  },
  {
    'leadingIcon': Icons.currency_exchange_outlined,
    'title': 'Currency',
    'subTitle': 'USD',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/currency',
  },
  {
    'leadingIcon': Icons.settings_outlined,
    'title': 'Setting',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/settings'
  },
];
// End Personal Screen

// Login Methods
List loginMethodContent = [
  {
    'icon': 'assets/google-icon.png',
    'text': 'Log in with Google',
  },
  {
    'icon': 'assets/facebook-icon.png',
    'text': 'Log in with Facebook',
  },
  {
    'icon': 'assets/email-icon.png',
    'text': 'Log in with Email',
  },
];
// End Login Methods

// Signup And Login
List otherLoginMethod = [
  {
    'icon': 'assets/google-icon.png',
    'text': 'Google',
  },
  {
    'icon': 'assets/facebook-icon.png',
    'text': 'Facebook',
  },
];
// End Signup And Login

// Home Scree - TopListView
List topListView = [
  Colors.grey[400],
  Colors.lightGreen,
  Colors.yellow,
  Colors.pink[300],
  Colors.blueAccent,
  Colors.green,
  Colors.orangeAccent,
  Colors.red[200],
];
// End Home Screen - TopListView

// Category Screen
List sideBarItem = [
  'Pick A Mood',
  'Sale',
  'New New',
  'Bestseller',
  'Tops',
  'Dresses',
  'Bottoms',
  'Matching sets',
  'Jumpsuits & Rompers',
  'Knitwear',
  'Swimwear',
  'Accessories',
  'Curve & Plus',
];

List pageItems = [
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-5.png',
        'name': 'Elegant',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-5.png',
        'name': 'Elegant',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-6.png',
        'name': 'Free',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-6.png',
        'name': 'Free',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-7.png',
        'name': 'K-pop',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-7.png',
        'name': 'K-pop',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-8.png',
        'name': 'Grunge',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-8.png',
        'name': 'Grunge',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-9.png',
        'name': 'High-street',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-9.png',
        'name': 'High-street',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      }
    ]
  },
];

// End Category Screen

// Shop By Category
List shopByCategory = [
  'assets/shop-by-category-bottom-img-1.png',
  'assets/shop-by-category-bottom-img-2.png',
  'assets/shop-by-category-bottom-img-3.png',
  'assets/shop-by-category-bottom-img-4.png',
];

List shopByCategoryTitle = [
  'Knitwear',
  'All Swimwear',
  'Matching Sets',
  'Curve & Plus',
];
// End Shop By Category

// Pick A Mood
List pickAMood = [
  Colors.pink[100],
  Colors.purple[400],
  Colors.pink,
  Colors.greenAccent[700],
  Colors.lightGreen[400],
  Colors.blueAccent,
  Colors.pinkAccent[400],
  Colors.lime[800],
  Colors.teal[400],
];
// End Pick A Mood

// Profile Screen
List profileListViewItems = [
  {
    'title': 'First Name',
    'subTitle': 'Thi',
  },
  {
    'title': 'Last Name',
    'subTitle': 'Cù Đình',
  },
  {
    'title': 'Nick Name',
    'subTitle': '3conhecon123@gmail.com',
  },
  {
    'title': 'Phone',
    'subTitle': '0902883759',
  },
];
// End Profile Screen

// Customer Service Screen
List serviceIntroduction = [
  {
    'icon': 'assets/cider-customer-service-whats-app-image.png',
    'title': 'WhatsApp',
    'subTitle': 'Enjoy a 24/7 Service',
  },
  {
    'icon': 'assets/cider-customer-service-contact-us-image.png',
    'title': 'Contact Us',
    'subTitle': 'Reply within 1 business day',
  },
  {
    'icon': 'assets/cider-customer-service-message-us-image.png',
    'title': 'Message Us',
    'subTitle': 'Reply within 24hrs',
  },
];
// End Customer Service Screen

// Language Screen
List language = [
  {'title': 'English', 'isSelected': true},
  {'title': 'Deutsch', 'isSelected': false},
  {'title': '한국인', 'isSelected': false},
  {'title': 'Francais', 'isSelected': false},
  {'title': 'Italiano', 'isSelected': false},
  {'title': 'Esperanto', 'isSelected': false},
];
// End Language Screen

// Settings Screen
List settings = [
  {
    'title': 'Clear Cache',
    'subTitle': '17.98MB',
  },
  {
    'title': 'Version',
    'subTitle': '1.9.3',
  },
];
// End Settings Screen

// Customer Service
List customerServices = [
  {
    'icon': Icons.text_snippet_outlined,
    'title': 'Order issue',
  },
  {
    'icon': Icons.local_shipping_outlined,
    'title': 'Shipping & Delivery',
  },
  {
    'icon': Icons.keyboard_return_outlined,
    'title': 'return & Refund',
  },
  {
    'icon': Icons.backpack_outlined,
    'title': 'Product & Stock',
  },
  {
    'icon': Icons.change_history_rounded,
    'title': 'Size & Fit',
  },
  {
    'icon': Icons.payment_outlined,
    'title': 'Payment & Promos',
  },
  {
    'icon': Icons.window_outlined,
    'title': 'Collab',
  },
  {
    'icon': Icons.question_mark_sharp,
    'title': 'Other inquires',
  },
];
// End Customer Service

// Currency Screen
List currency = [
  {
    'icon': 'assets/currency-listview-image-1.png',
    'title': "USD",
    'isSelected': true,
  },
  {
    'icon': 'assets/currency-listview-image-2.png',
    'title': "EUR",
    'isSelected': false,
  }
];
// End Currency Screen

// Top List View Items Screen
List topListViewItemsTitle = [
  'Curve & Plus',
  'The New New',
  'Everything',
  'Feeling Elegant',
  'Feeling Hot',
  'Feeling Dreamy',
  'Feeling Retro',
  'Feeling Cute',
];
// End Top List View Items Screen
