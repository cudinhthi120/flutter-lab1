import 'package:cloud_firestore/cloud_firestore.dart';

class ImageSlider {
  final String image;

  const ImageSlider({required this.image});

  static ImageSlider fromSnapshot(DocumentSnapshot snap) {
    ImageSlider images = ImageSlider(image: snap['image']);
    return images;
  }
}