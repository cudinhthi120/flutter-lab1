import 'package:cloud_firestore/cloud_firestore.dart';

class NewInProduct {
  final String image;
  final String name;
  final String price;
  final String colors;

  const NewInProduct({
    required this.image,
    required this.name,
    required this.price,
    required this.colors,
  });

  static NewInProduct fromSnapshot(DocumentSnapshot snap) {
    NewInProduct product = NewInProduct(
        image: snap['image'],
        name: snap['name'],
        price: snap['price'],
        colors: snap['colors'],
    );
    return product;
  }
}
