import 'package:cloud_firestore/cloud_firestore.dart';

class Inspiration {
  final String image;
  final String title;
  final String content;

  Inspiration({
    required this.image,
    required this.title,
    required this.content,
  });

  static Inspiration fromSnapshot(DocumentSnapshot snap) {
    Inspiration items = Inspiration(
      image: snap['image'],
      title: snap['title'],
      content: snap['content'],
    );
    return items;
  }
}
