import 'package:cloud_firestore/cloud_firestore.dart';

class FlashSaleProduct {
  final String name;
  final String image;
  final String newPrice;
  final String oldPrice;

  const FlashSaleProduct({
    required this.name,
    required this.image,
    required this.newPrice,
    required this.oldPrice,
  });

  static FlashSaleProduct fromSnapshot(DocumentSnapshot snap) {
    FlashSaleProduct product = FlashSaleProduct(
        name: snap['name'],
        image: snap['image'],
        newPrice: snap['newPrice'],
        oldPrice: snap['oldPrice']);
    return product;
  }
}
