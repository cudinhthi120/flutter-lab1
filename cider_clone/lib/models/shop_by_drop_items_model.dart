import 'package:cloud_firestore/cloud_firestore.dart';

class ShopByDropItems {
  final String title;
  final String subTitle;
  final String image;
  final String icon;

  ShopByDropItems({
    required this.title,
    required this.subTitle,
    required this.image,
    required this.icon,
  });

  static ShopByDropItems fromSnapshot(DocumentSnapshot snap) {
    ShopByDropItems items = ShopByDropItems(
      title: snap['title'],
      subTitle: snap['subTitle'],
      image: snap['image'],
      icon: snap['icon'],
    );
    return items;
  }
}
