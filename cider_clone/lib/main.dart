import 'package:cider_clone/services/google_sign_in.dart';
import 'views/ui/personal_screen/components/CountryScreen.dart';
import 'views/ui/personal_screen/PersonalScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'views/ui/search_screen/SearchScreen.dart';
import 'views/ui/shopping_cart/ShoppingCart.dart';
import 'views/ui/personal_screen/components/AddressBookScreen.dart';
import 'views/ui/personal_screen/components/CurrencyScreen.dart';
import 'views/ui/personal_screen/components/CustomerService.dart';
import 'views/ui/personal_screen/components/LanguageScreen.dart';
import 'views/ui/personal_screen/components/OrderScreen.dart';
import 'views/ui/personal_screen/components/ProfileScreen.dart';
import 'views/ui/personal_screen/components/SettingScreen.dart';
import 'views/ui/components/ListViewItemsScreen.dart';
import 'views/ui/components/ProductScreen.dart';
import 'views/ui/bottom_navigator/BottomNavigator.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => GoogleSignInProvider(),
        child: MaterialApp(
          title: 'Cider Clone',
          routes: {
            '/': (context) => BottomNavigator(),
            '/search': (context) => SearchScreen(),
            '/cart': (context) => ShoppingCart(),
            '/profile': (context) => ProfileScreen(),
            '/orders': (context) => OrderScreen(),
            '/address-book': (context) => AddressBookScreen(),
            '/customer-service': (context) => CustomerService(),
            '/language': (context) => LanguageScreen(),
            '/settings': (context) => SettingScreen(),
            '/currency': (context) => CurrencyScreen(),
            '/top-listview': (context) => TopListViewItemsScreen(),
            '/product': (context) => ProductScreen(),
            '/personal': (context) => PersonalScreen(),
            '/country': (context) => CountryScreen()
          },
          initialRoute: '/',
        ),
      );
}
