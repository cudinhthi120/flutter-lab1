import 'package:cider_clone/models/image_slider_model.dart';
import 'package:cider_clone/models/inspiration_model.dart';
import 'package:cider_clone/models/new_in_products_model.dart';
import 'package:cider_clone/models/shop_by_drop_items_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/flash_sale_products_model.dart';

class FirestoreDB {
  final FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Stream<List<NewInProduct>> getAllNewInProducts() {
    return _firebaseFirestore
        .collection('products')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => NewInProduct.fromSnapshot(doc))
          .toList();
    });
  }

  Stream<List<FlashSaleProduct>> getAllFlashSaleProducts() {
    return _firebaseFirestore
        .collection('FlashSaleProducts')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs
          .map((doc) => FlashSaleProduct.fromSnapshot(doc))
          .toList();
    });
  }

  Stream<List<ImageSlider>> getAllImageSlider() {
    return _firebaseFirestore
        .collection('ImageSlider')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => ImageSlider.fromSnapshot(doc)).toList();
    });
  }

  Stream<List<Inspiration>> getAllInspirationItems() {
    return _firebaseFirestore
        .collection('InspirationItems')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => Inspiration.fromSnapshot(doc)).toList();
    });
  }

  Stream<List<ShopByDropItems>> getAllShopByDropItems() {
    return _firebaseFirestore
        .collection('ShopByDropItems')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => ShopByDropItems.fromSnapshot(doc)).toList();
    });
  }
}
