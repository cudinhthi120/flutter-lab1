import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        body: Center(
          child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.green),
                foregroundColor: MaterialStateProperty.all(Colors.white),
              ),
              onPressed: () {
                MyBigNumber();
              },
              child: Text("Kết quả".toUpperCase())),
        ),
      ),
    );
  }
}

String MyBigNumber() {
  var logger = Logger();

  String strn1 = '02912';
  String strn2 = '18';
  int surplus = 0;
  int rememberNum = 0;
  List result = [];
  String finalResult = '';

// Reversed input String
  String reStr1 = strn1.split('').reversed.join('');
  String reStr2 = strn2.split('').reversed.join('');

// Split each character in String in to List
  List<String> str1 = reStr1.split('');
  List<String> str2 = reStr2.split('');

// Convert List of String to List of int
  List<int> num1 = str1.map(int.parse).toList();
  List<int> num2 = str2.map(int.parse).toList();

// Check which List of numbers has more character
// List with fewer characters will add 0 to the beginning of the list
//to balance the characters in two List
  int check1 = num1.length - num2.length;
  int check2 = num2.length - num1.length;

  for (int i = 0; i < check1; i++) {
    num2.add(0);
  }

  for (int i = 0; i < check2; i++) {
    num1.add(0);
  }

  logger.d(num1);
  logger.d(num2);

  for (int i = 0; i < num1.length; i++) {
    if ((num1[i] + num2[i] + rememberNum) == 10) {
      surplus = 0;
    } else {
      surplus = (num1[i] + num2[i]) % 10 + rememberNum;
    }
    logger.d('dư $surplus');

    rememberNum = (num1[i] + num2[i]) ~/ 10;
    logger.d('nhớ $rememberNum');

    //  Add surplus to result
    result.add(surplus);

    // Check if it's the last character
    if (i == num1.length - 1) {
      result.add(rememberNum);
    }
    logger.d("kết quả: $result");
  }

  // Convert List of int to String
  finalResult = (int.parse(result.reversed.join('')).toString());
  logger.d('kết quả cuối: $finalResult');

  return finalResult;
}
