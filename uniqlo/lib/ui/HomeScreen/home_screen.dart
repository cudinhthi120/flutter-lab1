import 'package:flutter/material.dart';
import 'package:uniqlo/ui/search_screen.dart';
import '../cart_screen.dart';
import 'home_screen_tabbar_item.dart';
import '../notification_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabs = [
      {"text": "TOP"},
      {"text": "WOMEN"},
      {"text": "MEN"},
      {"text": "KIDS"},
      {"text": "BABY"},
    ];
    return SafeArea(
      child: Column(
        children: [
          Container(
            color: Colors.white,
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SearchScreen(),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(4)),
                      margin: EdgeInsets.only(left: 8, right: 8, top: 8),
                      padding: const EdgeInsets.only(
                          left: 16, right: 16, top: 12, bottom: 12),
                      child: Row(
                        children: [
                          Icon(
                            Icons.search,
                            color: Colors.grey,
                          ),
                          SizedBox(width: 8),
                          Expanded(
                            flex: 1,
                            child: Text(
                              'Bạn đang tìm sản phẩm gì ?',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(color: Colors.grey, fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(NotificationScreen.route);
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 18),
                    child: Icon(Icons.message_outlined),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(CartScreen.route);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16, right: 30),
                    child: Icon(Icons.shopping_cart_outlined),
                  ),
                ),
              ],
            ),
          ),
          DefaultTabController(
            length: tabs.length,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300]!,
                      blurRadius: 1,
                      offset: Offset(0, 2),
                    ),
                  ]),
                  child: TabBar(
                    indicatorWeight: 2,
                    indicatorColor: Colors.black,
                    unselectedLabelColor: Colors.grey[500],
                    labelColor: Colors.black,
                    labelStyle:
                        TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                    tabs: tabs
                        .map((tab) => Tab(
                              text: tab['text'],
                            ))
                        .toList(),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height - 184,
                  child: TabBarView(
                    children: tabs.map((tab) {
                      return TabBarItem();
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
