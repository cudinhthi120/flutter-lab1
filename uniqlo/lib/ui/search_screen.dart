import 'package:flutter/material.dart';

class SearchScreen extends StatelessWidget {
  static const route = '/search';

  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[300]!,
                    blurRadius: 1,
                    offset: Offset(0, 1),
                  ),
                ],
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                child: TextField(
                  textAlignVertical: TextAlignVertical.center,
                  cursorColor: Colors.red,
                  cursorWidth: 3,
                  cursorHeight: 20,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey[200],
                    enabledBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.grey),
                    prefixIcon: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.grey,
                      ),
                    ),
                    hintText: 'Bạn đan tìm sản phẩm gì ?',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 32, bottom: 32, left: 48, right: 64),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      'Lich sử tìm kiếm'.toUpperCase(),
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Icon(
                    Icons.restore_from_trash_outlined,
                    size: 26,
                  ),
                ],
              ),
            ),
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 3,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.only(left: 48, right: 72, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'trẻ nhỏ (6 tháng - 5 năm'.toUpperCase(),
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(height: 3),
                          Text(
                            'Trẻ sơ sinh'.toUpperCase(),
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                            ),
                          )
                        ],
                      ),
                    ),
                    Icon(Icons.close, size: 15, color: Colors.grey,),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
