import 'package:flutter/material.dart';

class CartScreen extends StatelessWidget {
  static const route = '/cart';

  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text('Xe đẩy'.toUpperCase()),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      backgroundColor: Colors.grey[200],
      body: Column(
        children: [
          Container(
            width: double.infinity,
            color: Colors.grey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 8),
              child: Text(
                'Thuế suất thuế gtgt áp dụng cho sản phẩm của chúng tôi sẽ giảm 2%, và giá niêm yết của tất cả sản phẩm là giá đã bao gồm 8% thuế GTGT.'
                    .toUpperCase(),
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 1,
                  color: Colors.grey,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'Giỏ hàng'.toUpperCase(),
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16,
                    right: 16,
                    top: 16,
                    bottom: 24,
                  ),
                  child: Text(
                    'Không có sản phẩm nào trong giỏ hàng của bạn',
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.black,
                  ),
                  width: double.infinity,
                  margin: EdgeInsets.all(16),
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Tiếp tục mua sắm'.toUpperCase(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
