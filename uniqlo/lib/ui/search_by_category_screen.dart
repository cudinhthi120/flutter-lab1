import 'package:flutter/material.dart';
import 'package:uniqlo/data.dart';
import 'components/search_bar.dart';

class SearchByCategoryScreen extends StatelessWidget {
  const SearchByCategoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabs = [
      {"text": "NỮ"},
      {"text": "NAM"},
      {"text": "TRẺ EM"},
      {"text": "TRẺ SƠ SINH"},
    ];
    return SafeArea(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 1,
                    offset: Offset(0, 1),
                  ),
                ],
              ),
              child: Container(
                  margin:
                      EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
                  child: SearchBar()),
            ),
            DefaultTabController(
              length: tabs.length,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: EdgeInsets.all(8),
                    color: Colors.white,
                    child: TabBar(
                      labelPadding: EdgeInsets.zero,
                      indicatorWeight: 3,
                      indicatorColor: Colors.black,
                      unselectedLabelColor: Colors.grey[500],
                      labelColor: Colors.black,
                      labelStyle:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w500, ),
                      tabs: tabs
                          .map((tab) => Tab(
                                text: tab['text'],
                              ))
                          .toList(),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height -219,
                    child: TabBarView(
                      children: [
                        TabScreen(count: 9, number: 0),
                        TabScreen(count: 7, number: 1),
                        TabScreen(count: 8, number: 2),
                        TabScreen(count: 3, number: 3),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TabScreen extends StatelessWidget {
  final int count, number;
  const TabScreen({
    Key? key,
     required this.count, required this.number,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: count,
      shrinkWrap: true,
      itemBuilder: (context, index) => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1, color: Colors.grey[350]!),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              pageItems[number]['items'][index],
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
