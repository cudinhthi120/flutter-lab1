import 'package:flutter/material.dart';
import '../favorite_screen.dart';
import '../MemberScreen/member_screen.dart';
import '../scan_screen.dart';
import '../search_by_category_screen.dart';
import '../HomeScreen/home_screen.dart';

class BotNavigationBar extends StatefulWidget {
  static const route = '/';

  const BotNavigationBar({Key? key}) : super(key: key);

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<BotNavigationBar> {
  int currentIndex = 0;
  List items = [
    {
      'label': 'Trang chủ',
      'icon': Icons.home,
    },
    {
      'label': 'Tìm kiếm',
      'icon': Icons.search,
    },
    {
      'label': 'Yêu thích',
      'icon': Icons.favorite_border_outlined,
    },
    {
      'label': 'Quét',
      'icon': Icons.settings_overscan_outlined,
    },
    {
      'label': 'Thành viên',
      'icon': Icons.credit_card_outlined,
    },
  ];
  final List screens = [
    const HomeScreen(),
    const SearchByCategoryScreen(),
    const FavoriteScreen(),
    const ScanScreen(),
    const MemberScreen(),
  ];

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTap,
        elevation: 0,
        currentIndex: currentIndex,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        items: items
            .map((item) => BottomNavigationBarItem(
            icon: Icon(item['icon']), label: item['label']))
            .toList(),
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.black45,
      ),
    );
  }
}
