import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SurveyScreen extends StatelessWidget {
  static const route = '/survey';

  const SurveyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 3,
        title: Text(
          'Lưu bảng câu hỏi khảo sát dịch vụ'.toUpperCase(),
          style: TextStyle(overflow: TextOverflow.ellipsis, wordSpacing: 1),
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[500]!,
                  offset: Offset(0, 1),
                ),
              ],
            ),
            padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 8),
            child: Row(
              children: [
                Image.asset('assets/survey-img.png'),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text(
                    'UNIQLO Trợ giúp',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                Icon(
                  Icons.menu,
                  size: 35,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 1),
            padding: EdgeInsets.only(left: 16, right: 16, top: 32, bottom: 48),
            color: Colors.white,
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  color: Colors.grey[300],
                  width: 110,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Padding(
                          padding:
                              const EdgeInsets.only(left: 6, top: 6, bottom: 6),
                          child: Text('Tiếng Việt'),
                        ),
                      ),
                      Icon(Icons.arrow_drop_down_outlined),
                    ],
                  ),
                ),
                Container(
                  height: 40,
                  margin: EdgeInsets.only(top: 12),
                  child: TextField(
                    autofocus: false,
                    cursorWidth: 1,
                    cursorColor: Colors.black,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 16),
                      hintText: 'Tìm kiếm Câu Hỏi Thường Gặp',
                      hintStyle: TextStyle(color: Colors.black, fontSize: 14),
                      suffixIcon: Icon(Icons.search),
                      suffixIconColor: Colors.black,
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 1,
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.zero),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            width: 1,
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.zero),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 2),
            padding: EdgeInsets.all(16),
            color: Colors.white,
            child: Row(
              children: [
                Text(
                  'Trợ giúp',
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
                SizedBox(
                  width: 6,
                ),
                Text('/'),
                SizedBox(
                  width: 6,
                ),
                Text(
                  'Cửa hàng',
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
                SizedBox(
                  width: 6,
                ),
                Text('/'),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Q. Khảo sát dịch vụ tại cửa hàng',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 16),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
