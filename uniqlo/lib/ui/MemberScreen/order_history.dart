import 'package:flutter/material.dart';

class OrderHistory extends StatelessWidget {
  static const route = '/order-history';

  const OrderHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lịch sử đơn hàng'.toUpperCase()),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      backgroundColor: Colors.grey[200],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text(
              'lịch sử đơn hàng'.toUpperCase(),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Text(
              'Bạn có thể hủy đơn hàng trong vòng 30 phút bằng cách nhấn "Hủy đơn hàng" sau khi đơn hàng của bạn đã dược đặt. Đơn hàng của bạn sẽ tự động bị hủy nếu trạng thái giao hàng được ghi là chưa hoàn thành.',
              style: TextStyle(fontSize: 15),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 8),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.grey[400]!,
                blurRadius: 1,
                offset: Offset(0, 2),
              ),
            ]),
            child: Text('Chúng tôi không có đơn hàng nào cho tài khoản này.'),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(16),
            color: Colors.black,
            child: TextButton(
              onPressed: () {},
              child: Text(
                'Quay lại thành viên'.toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
