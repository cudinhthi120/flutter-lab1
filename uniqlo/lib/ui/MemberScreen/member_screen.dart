import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MemberScreen extends StatelessWidget {
  const MemberScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        title: Text(
          'Thành viên'.toUpperCase(),
        ),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      backgroundColor: Colors.grey[200],
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.all(16),
                  height: 200,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 44, vertical: 22),
                    child: Image.asset(
                      'assets/member-top-container-img.png',
                      fit: BoxFit.fill,
                    ),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      Icons.brightness_4_outlined,
                      color: Colors.blue[900],
                    ),
                  ),
                  Text(
                    'Độ sáng'.toUpperCase(),
                    style: TextStyle(color: Colors.blue[900]),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  children: [
                    BodyButtonWithIcon(
                      title: 'Mã giảm giá (yêu cầu đăng nhập)',
                      icon: Icons.discount_outlined,
                      route: '/coupons',
                    ),
                    BreakLine(),
                    BodyButtonWithIcon(
                      title: 'Lưu bảng câu hỏi khảo sát dịch vụ',
                      icon: Icons.note_add_outlined,
                      route: '/survey',
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 32, top: 16, bottom: 16),
                      child: Text('Lịch sử'.toUpperCase()),
                    ),
                    BodyButtonWithIcon(
                      title: 'Lịch sử mua hàng',
                      icon: Icons.shopping_bag_outlined,
                      route: '/purchase-history',
                    ),
                    BreakLine(),
                    BodyButtonWithIcon(
                      title: 'Lịch sử đơn hàng',
                      icon: Icons.move_to_inbox_outlined,
                      route: '/order-history',
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 32, top: 16, bottom: 16),
                      child: Text('Cài đặt'.toUpperCase()),
                    ),
                    BodyButtonWithIcon(
                      title: 'cài đặt hồ sơ',
                      icon: Icons.person_outline_outlined,
                      route: '/profile-setting',
                    ),
                    BreakLine(),
                    BodyButtonWithIcon(
                      title: 'cài đặt ứng dụng',
                      icon: Icons.settings_outlined,
                      route: '/app-setting',
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 16, bottom: 16),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 1,
                        offset: Offset(0, 2)),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                        color: Colors.white,
                        alignment: Alignment.topLeft,
                        child: Text('Khác'.toUpperCase())),
                    ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        BodyButtonWithoutIcon(
                          title: 'câu hỏi thường gặp',
                        ),
                        BodyButtonWithoutIcon(
                          title: 'điều khoản sử dụng',
                        ),
                        BodyButtonWithoutIcon(
                          title: 'chính sách bảo mật',
                        ),
                        BodyButtonWithoutIcon(
                          title: 'phiên bản: 7.2.33',
                        ),
                        BodyButtonWithoutIcon(
                          title: 'giấy phep oss',
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class BodyButtonWithIcon extends StatelessWidget {
  final String title;
  final IconData icon;
  final String route;

  const BodyButtonWithIcon({
    Key? key,
    required this.title,
    required this.icon,
    required this.route,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextButton(
        onPressed: () {
          Navigator.of(context).pushNamed(route);
        },
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 8,
                horizontal: 16,
              ),
              child: Icon(
                icon,
                color: Colors.black,
              ),
            ),
            Text(
              title.toUpperCase(),
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BodyButtonWithoutIcon extends StatelessWidget {
  final String title;

  const BodyButtonWithoutIcon({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          child: TextButton(
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.only(left: 16),
              alignment: Alignment.topLeft,
              child: Text(
                title.toUpperCase(),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 16,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 1,
          color: Colors.grey[400],
        )
      ],
    );
  }
}

class BreakLine extends StatelessWidget {
  const BreakLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1,
      color: Colors.grey[400],
      margin: EdgeInsets.only(left: 64),
    );
  }
}
