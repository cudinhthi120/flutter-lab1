import 'package:flutter/material.dart';
import 'cart_screen.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text('Yêu thích'),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(CartScreen.route);
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 16),
              child: Icon(Icons.shopping_cart_outlined),
            ),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Container(
            height: 60,
            color: Colors.white,
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 16),
                    child: Text('0 (Các) sản phẩm'),
                  ),
                ),
                Icon(Icons.category_outlined),
                Padding(
                  padding: const EdgeInsets.only(right: 16, left: 22),
                  child: Icon(Icons.edit_outlined),
                ),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 32, horizontal: 64),
              child: Text(
                'Có 0 sản phẩm trong danh sách mong muốn'.toUpperCase(),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              ),
            ),
            Image.asset('assets/favorite-body-img.png'),
            Container(
              padding: EdgeInsets.only(top: 32, left: 16, right: 16),
              child: Text(
                'Thêm sản phẩm vào danh sách mong muốn và kiểm tra giá cùng với thông timn hàng tồn kho',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
