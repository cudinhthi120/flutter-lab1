import 'package:flutter/material.dart';
import 'package:mvc/controllers/cart_controller.dart';
import 'package:mvc/models/product_model.dart';
import 'package:get/get.dart';

class CatalogProduct extends StatelessWidget {
  const CatalogProduct({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
          itemCount: Product.products.length,
          itemBuilder: (BuildContext context, int index) {
            return CatalogProductCard(index: index);
          }),
    );
  }
}

class CatalogProductCard extends StatelessWidget {
  final int index;
  final cartController = Get.put(CartController());
  CatalogProductCard({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            radius: 40,
            backgroundImage: NetworkImage(
              Product.products[index].imageUrl,
            ),
          ),
          Text(Product.products[index].name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
          Text('${Product.products[index].price}', style: TextStyle(fontWeight: FontWeight.bold),),
          IconButton(
            onPressed: () {
              cartController.addProduct(Product.products[index]);
            },
            icon: Icon(Icons.add_circle),
          ),
        ],
      ),
    );
  }
}
