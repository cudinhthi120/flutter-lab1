class Product {
  final String name;
  final double price;
  final String imageUrl;

  const Product({
    required this.name,
    required this.price,
    required this.imageUrl,
  });

  static const List<Product> products = [
    Product(
      name: 'Apple',
      price: 2.99,
      imageUrl:
          'https://img2.thuthuatphanmem.vn/uploads/2019/03/14/hinh-ve-qua-tao-cuc-dep_095352955.png',
    ),
    Product(
      name: 'Orange',
      price: 2.99,
      imageUrl: 'https://o.rada.vn/data/image/2021/03/26/Ta-qua-cam-700.jpg',
    ),
    Product(
      name: 'Pear',
      price: 2.99,
      imageUrl:
          'https://haycafe.vn/wp-content/uploads/2022/03/anh-qua-dao-1.jpg',
    ),
  ];
}
