import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mvc/widgets/catalog_products.dart';

import 'cart_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shop'),
        centerTitle: true,
        elevation: 0,
      ),
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CatalogProduct(),
              ElevatedButton(
                onPressed: () => Get.to(() => CartScreen()),
                child: Text('Go to cart'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
