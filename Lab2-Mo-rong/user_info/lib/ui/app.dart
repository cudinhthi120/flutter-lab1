import 'package:flutter/material.dart';
import 'package:user_info/validation/validation.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Get user info",
      home: Scaffold(
        appBar:  AppBar(title: Text('Get user info')),
        body: Container(
          padding: EdgeInsets.all(24.0),
          child: GetInfoScreen(),
        ),
      ),
    );
  }
}

class GetInfoScreen extends StatefulWidget {
  const GetInfoScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return InfoState();
  }
}

class InfoState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;
  late String firstName;
  late String lastName;
  late String birth;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                fieldFirstName(),
                Container(margin: EdgeInsets.only(top: 20.0),),
                fieldLastName(),
                Container(margin: EdgeInsets.only(top: 40.0),),
                fieldEmailAddress(),
                Container(margin:  EdgeInsets.only(top: 40.0),),
                fieldBirth(),
                Container(margin:  EdgeInsets.only(top: 40.0),),
                fieldCountryOptions(),
                Container(margin:  EdgeInsets.only(top: 40.0),),
                fieldCityOptions(),
                Container(margin:  EdgeInsets.only(top: 40.0),),
                fieldDistrictOptions(),
                Container(margin:  EdgeInsets.only(top: 40.0),),
                confirmButton(),
              ],
            ),
          ),
        )
    );
  }

  Widget fieldLastName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Last Name'
      ),
      validator: validateLastName,
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'First Name'
      ),
      validator: validateFirstName,
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email_outlined),
          labelText: 'Email address',
          hintText: 'email@gmail.com'
      ),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldBirth() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          icon: Icon(Icons.calendar_today_outlined),
          labelText: "Birth"
      ),
      validator: validateBirth,
      onSaved:  (value) {
        birth = value as String;
      },
    );
  }

  List<DropdownMenuItem<String>> get dropdownCountries {
    List<DropdownMenuItem<String>> countries = [
      DropdownMenuItem(child: Text("Việt Nam"), value: "Viet Nam"),
      DropdownMenuItem(child: Text("Campuchia"), value: "Campuchia"),
      DropdownMenuItem(child: Text("Nhật Bản"), value: "Nhat Ban"),
      DropdownMenuItem(child: Text("Hàn Quốc"), value: "Han Quoc"),
      DropdownMenuItem(child: Text("Ấn Độ"), value: "An Do"),
      DropdownMenuItem(child: Text("Canada"), value: "Canada"),
      DropdownMenuItem(child: Text("Brazil"), value: "Brazil"),
      DropdownMenuItem(child: Text("England"), value: "England"),
    ];
    return countries;
  }

  String selectedCountry = "Viet Nam";

  Widget fieldCountryOptions() {
    return DropdownButtonFormField(
        decoration: InputDecoration(
          labelText: 'Chọn quốc gia',
          labelStyle: TextStyle(fontSize: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        dropdownColor: Colors.white,
        value: selectedCountry,
        onChanged: (String? newValue) {
          setState(() {
            selectedCountry = newValue!;
          });
        },
        items: dropdownCountries);
  }

  List<DropdownMenuItem<String>> get dropdownCities {
    List<DropdownMenuItem<String>> cities = [
      DropdownMenuItem(child: Text("TP. Hồ Chí Minh"), value: "TP.HCM"),
      DropdownMenuItem(child: Text("Vũng Tàu"), value: "VungTau"),
      DropdownMenuItem(child: Text("Huế"), value: "Hue"),
      DropdownMenuItem(child: Text("Hà Nội"), value: "HaNoi"),
      DropdownMenuItem(child: Text("Quy Nhơn"), value: "QuyNhon"),
      DropdownMenuItem(child: Text("Đà Lạt"), value: "DaLat"),
      DropdownMenuItem(child: Text("Bạc Liêu"), value: "BacLieu"),
      DropdownMenuItem(child: Text("Bến Tre"), value: "BenTre"),
    ];
    return cities;
  }

  String selectedCity = "TP.HCM";

  Widget fieldCityOptions() {
    return DropdownButtonFormField(
        decoration: InputDecoration(
          labelText: 'Chọn Tỉnh/Thành phố',
          labelStyle: TextStyle(fontSize: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        dropdownColor: Colors.white,
        value: selectedCity,
        onChanged: (String? newValue) {
          setState(() {
            selectedCity = newValue!;
          });
        },
        items: dropdownCities);
  }

  List<DropdownMenuItem<String>> get dropdownDistricts {
    List<DropdownMenuItem<String>> district = [
      DropdownMenuItem(child: Text("Quận 1"), value: "q1"),
      DropdownMenuItem(child: Text("Quận 2"), value: "q2"),
      DropdownMenuItem(child: Text("Quận 3"), value: "q3"),
      DropdownMenuItem(child: Text("Quận 4"), value: "q4"),
      DropdownMenuItem(child: Text("Quận 5"), value: "q5"),
      DropdownMenuItem(child: Text("Quận 7"), value: "q7"),
      DropdownMenuItem(child: Text("Quận 8"), value: "q8"),
      DropdownMenuItem(child: Text("Quận 10"), value: "q10"),
    ];
    return district;
  }

  String selectedDistrict = "q1";

  Widget fieldDistrictOptions() {
    return DropdownButtonFormField(
        decoration: InputDecoration(
          labelText: 'Chọn quận/huyện',
          labelStyle: TextStyle(fontSize: 20),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.lightBlue, width: 2),
            borderRadius: BorderRadius.circular(0),
          ),
          filled: true,
          fillColor: Colors.white,
        ),
        dropdownColor: Colors.white,
        value: selectedDistrict,
        onChanged: (String? newValue) {
          setState(() {
            selectedDistrict = newValue!;
          });
        },
        items: dropdownDistricts);
  }

  Widget confirmButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $firstName, $lastName, $birth, $selectedCountry, $selectedCity, $selectedDistrict');
          }
        },
        child: Text('confirm'.toUpperCase()),
    );
  }
}
