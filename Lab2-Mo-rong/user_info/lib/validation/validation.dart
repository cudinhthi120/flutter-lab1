mixin CommonValidation {
  String? validateLastName(String? value) {
    if (value!.isEmpty) {
      return 'Please enter your last name.';
    }
    return null;
  }

  String? validateFirstName(String? value) {
    if (value!.isEmpty) {
      return 'Please enter your first name';
    }
    return null;
  }

  String? validateBirth(String? value) {
    if (value!.isEmpty) {
      return "Please enter your birth";
    }
    if (!value.contains(RegExp(r'[0-9]')) || int.parse(value) > 2022 || int.parse(value) < 1900) {
      return "Please enter valid birth";
    }
    return null;
  }

  String? validateEmail(String? value) {
    if(value!.isEmpty){
      return 'Please input your email';
    }
    if (!value.contains('@')) {
      return 'Please input valid email.';
    }
    return null;
  }
}
