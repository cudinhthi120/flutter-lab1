import 'package:flutter/material.dart';

// Personal Screen
IconData leadingIcon = Icons.account_circle_outlined;
var title = 'My Profile';
var subTitle = '';
IconData trailingIcon = Icons.keyboard_arrow_right;

List firstListViewItems = [
  {
    'leadingIcon': Icons.calendar_today_outlined,
    'title': 'Order',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/orders',
  },
  {
    'leadingIcon': Icons.location_on_outlined,
    'title': 'Address Book',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/address-book',
  },
  {
    'leadingIcon': Icons.headset_outlined,
    'title': 'Customer Service',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/customer-service',
  },
];

List secondLisViewItems = [
  {
    'leadingIcon': Icons.language_outlined,
    'title': 'Country/Region',
    'subTitle': 'United States',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/language'
  },
  {
    'leadingIcon': Icons.chat_outlined,
    'title': 'Language',
    'subTitle': 'English',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/language'
  },
  {
    'leadingIcon': Icons.currency_exchange_outlined,
    'title': 'Currency',
    'subTitle': 'USD',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/currency',
  },
  {
    'leadingIcon': Icons.settings_outlined,
    'title': 'Setting',
    'subTitle': '',
    'trailingIcon': Icons.keyboard_arrow_right,
    'route': '/settings'
  },
];
// End Personal Screen

// Login Methods
List loginMethodContent = [
  {
    'icon': 'assets/google-icon.png',
    'text': 'Log in with Google',
  },
  {
    'icon': 'assets/facebook-icon.png',
    'text': 'Log in with Facebook',
  },
  {
    'icon': 'assets/email-icon.png',
    'text': 'Log in with Email',
  },
];
// End Login Methods

// Signup And Login
List otherLoginMethod = [
  {
    'icon': 'assets/google-icon.png',
    'text': 'Google',
  },
  {
    'icon': 'assets/facebook-icon.png',
    'text': 'Facebook',
  },
];
// End Signup And Login

// Home Scree - TopListView
List topListView = [
  {
    'image': Colors.grey[400],
    'title': 'Curve',
  },
  {
    'image': Colors.lightGreen,
    'title': 'New',
  },
  {
    'image': Colors.yellow,
    'title': 'Everything',
  },
  {
    'image': Colors.pink[300],
    'title': 'Elegant',
  },
  {
    'image': Colors.blueAccent,
    'title': 'Hot',
  },
  {
    'image': Colors.green,
    'title': 'Dreamy',
  },
  {
    'image': Colors.orangeAccent,
    'title': 'Retro',
  },
  {
    'image': Colors.red[200],
    'title': 'Cute',
  },
];
// End Home Screen - TopListView

// Home Screen - Inspiration
List inspiration = [
  {
    'image': 'assets/inspiration-second-img.png',
    'title': 'Old Money Aesthetic',
    'content':
        'Shop the timeless items that are always classic and under-stated elegance',
  },
  {
    'image': 'assets/inspiration-img.png',
    'title': 'Bold Orange',
    'content':
        'Orange can sometimes be a challenging color to wear, but this summer, we are showing you how to style in a bold way!',
  },
];
// End Home Screen - Inspiration

// Home Screen - NewIn
List newIn = [
  {
    'image': 'assets/new-in-listview-items-1.png',
    'name': 'Boho Paisley Patchwork Halter Top',
    'price': 'USS18.00',
    'color': '1 colour'
  },
  {
    'image': 'assets/new-in-listview-items-2.png',
    'name': 'Trumpet Flowers Corset Halter Top',
    'price': 'USS18.00',
    'color': '1 colour'
  },
  {
    'image': 'assets/new-in-listview-items-3.png',
    'name': 'Solid Lace Up Tank Top',
    'price': 'USS32.00',
    'color': '1 colour'
  },
  {
    'image': 'assets/new-in-listview-items-4.png',
    'name': 'Lava Lamp Split Multicolor Cardigan',
    'price': 'USS18.00',
    'color': '1 colour'
  },
  {
    'image': 'assets/new-in-listview-items-5.png',
    'name': 'Tie Dye Tie Back Top',
    'price': 'USS10.00',
    'color': '1 colour'
  },
  {
    'image': 'assets/new-in-listview-items-6.png',
    'name': 'Cute Yellow Short Sleeve Blouse',
    'price': 'USS24.00',
    'color': '1 colour'
  },
];
// End NewIn

// Home Screen - Daily Flash Sale
List dailyFlashSale = [
  {
    'image': 'assets/daily-flash-sale-listview-items-1.png',
    'name': 'Floral Ruched Halter',
    'newPrice': 'USS12.00',
    'oldPrice': 'USS16.00'
  },
  {
    'image': 'assets/daily-flash-sale-listview-items-2.png',
    'name': 'Landscape Painting Ruched Skirt',
    'newPrice': 'USS10.00',
    'oldPrice': 'USS18.00'
  },
  {
    'image': 'assets/daily-flash-sale-listview-items-3.png',
    'name': 'Heart & Floral Embroidery Denim Shorts',
    'newPrice': 'USS10.00',
    'oldPrice': 'USS16.00'
  },
  {
    'image': 'assets/daily-flash-sale-listview-items-4.png',
    'name': 'Floral Shirred Ruffle Tank Top',
    'newPrice': 'USS15.00',
    'oldPrice': 'USS32.00'
  },
  {
    'image': 'assets/daily-flash-sale-listview-items-5.png',
    'name': '3 Piece Bikini Top With Shirt & Skirt Set',
    'newPrice': 'USS10.00',
    'oldPrice': 'USS18.00'
  },
  {
    'image': 'assets/daily-flash-sale-listview-items-6.png',
    'name': 'Patchy Eyelet Embroidery Gingham Cami Dress',
    'newPrice': 'USS8.00',
    'oldPrice': 'USS20.00'
  },
];
// End Home Screen - Daily Flash Sale

// Category Screen
List sideBarItem = [
  'Pick A Mood',
  'Sale',
  'New New',
  'Bestseller',
  'Tops',
  'Dresses',
  'Bottoms',
  'Matching sets',
  'Jumpsuits & Rompers',
  'Knitwear',
  'Swimwear',
  'Accessories',
  'Curve & Plus',
];

List pageItems = [
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-5.png',
        'name': 'Elegant',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-5.png',
        'name': 'Elegant',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-6.png',
        'name': 'Free',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-6.png',
        'name': 'Free',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-7.png',
        'name': 'K-pop',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-7.png',
        'name': 'K-pop',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-8.png',
        'name': 'Grunge',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-8.png',
        'name': 'Grunge',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-9.png',
        'name': 'High-street',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-9.png',
        'name': 'High-street',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-1.png',
        'name': 'Cute',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-2.png',
        'name': 'Retro',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-3.png',
        'name': 'Hot',
      }
    ]
  },
  {
    'items': [
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      },
      {
        'image': 'assets/pick-a-mood-gridview-img-4.png',
        'name': 'Dreamy',
      }
    ]
  },
];

// End Category Screen

// Shop By Category
List shopByCategory = [
  'assets/shop-by-category-bottom-img-1.png',
  'assets/shop-by-category-bottom-img-2.png',
  'assets/shop-by-category-bottom-img-3.png',
  'assets/shop-by-category-bottom-img-4.png',
];

List shopByCategoryTitle = [
  'Knitwear',
  'All Swimwear',
  'Matching Sets',
  'Curve & Plus',
];
// End Shop By Category

// Shop by Drops
List shopByDrops = [
  {
    'image': 'assets/shop-by-drop-listview-img-1.png',
    'title': 'Prom 22',
    'icon': 'assets/shop-by-drop-listview-icon-1.png',
    'subTitle': 'The Formal Edit',
  },
  {
    'image': 'assets/shop-by-drop-listview-img-2.png',
    'title': 'Pick A Mood, Pick A Song',
    'icon': 'assets/shop-by-drop-listview-icon-2.png',
    'subTitle': 'The Music Festival Edit',
  },
  {
    'image': 'assets/shop-by-drop-listview-img-3.png',
    'title': 'Spring Daydream',
    'icon': 'assets/shop-by-drop-listview-icon-3.png',
    'subTitle': 'Pre Spring',
  },
  {
    'image': 'assets/shop-by-drop-listview-img-4.png',
    'title': 'Bloom Bloom Pow',
    'icon': 'assets/shop-by-drop-listview-icon-4.png',
    'subTitle': 'The Spring Edit',
  },
  {
    'image': 'assets/shop-by-drop-listview-img-5.png',
    'title': 'Love Wonderland',
    'icon': 'assets/shop-by-drop-listview-icon-5.png',
    'subTitle': 'Spring 22 Campaign',
  },
];
// End Shop by Drops

// Pick A Mood
List pickAMood = [
  {
    'image': 'assets/pick-a-mood-gridview-img-1.png',
    'color': Colors.pink[100],
    'title': 'Cute',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-2.png',
    'color': Colors.purple[400],
    'title': 'Retro',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-3.png',
    'color': Colors.pink,
    'title': 'Hot',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-4.png',
    'color': Colors.greenAccent[700],
    'title': 'Dreamy',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-5.png',
    'color': Colors.lightGreen[400],
    'title': 'Elegant',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-6.png',
    'color': Colors.blueAccent,
    'title': 'Free',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-7.png',
    'color': Colors.pinkAccent[400],
    'title': 'K-pop',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-8.png',
    'color': Colors.lime[800],
    'title': 'Grunge',
  },
  {
    'image': 'assets/pick-a-mood-gridview-img-9.png',
    'color': Colors.teal[400],
    'title': 'High-street',
  },
];
// End Pick A Mood

// Profile Screen
List profileListViewItems = [
  {
    'title': 'First Name',
    'subTitle': 'Thi',
  },
  {
    'title': 'Last Name',
    'subTitle': 'Cù Đình',
  },
  {
    'title': 'Nick Name',
    'subTitle': '3conhecon123@gmail.com',
  },
  {
    'title': 'Phone',
    'subTitle': '0902883759',
  },
];
// End Profile Screen

// Customer Service Screen
List serviceIntroduction = [
  {
    'icon': 'assets/cider-customer-service-whats-app-image.png',
    'title': 'WhatsApp',
    'subTitle': 'Enjoy a 24/7 Service',
  },
  {
    'icon': 'assets/cider-customer-service-contact-us-image.png',
    'title': 'Contact Us',
    'subTitle': 'Reply within 1 business day',
  },
  {
    'icon': 'assets/cider-customer-service-message-us-image.png',
    'title': 'Message Us',
    'subTitle': 'Reply within 24hrs',
  },
];
// End Customer Service Screen

// Language Screen
List language = [
  {'title': 'English', 'isSelected': true},
  {'title': 'Deutsch', 'isSelected': false},
  {'title': '한국인', 'isSelected': false},
  {'title': 'Francais', 'isSelected': false},
  {'title': 'Italiano', 'isSelected': false},
  {'title': 'Esperanto', 'isSelected': false},
];
// End Language Screen

// Settings Screen
List settings = [
  {
    'title': 'Clear Cache',
    'subTitle': '17.98MB',
  },
  {
    'title': 'Version',
    'subTitle': '1.9.3',
  },
];
// End Settings Screen

// Customer Service
List customerServices = [
  {
    'icon': Icons.text_snippet_outlined,
    'title': 'Order issue',
  },
  {
    'icon': Icons.local_shipping_outlined,
    'title': 'Shipping & Delivery',
  },
  {
    'icon': Icons.keyboard_return_outlined,
    'title': 'return & Refund',
  },
  {
    'icon': Icons.backpack_outlined,
    'title': 'Product & Stock',
  },
  {
    'icon': Icons.change_history_rounded,
    'title': 'Size & Fit',
  },
  {
    'icon': Icons.payment_outlined,
    'title': 'Payment & Promos',
  },
  {
    'icon': Icons.window_outlined,
    'title': 'Collab',
  },
  {
    'icon': Icons.question_mark_sharp,
    'title': 'Other inquires',
  },
];
// End Customer Service

// Currency Screen
List currency = [
  {
    'icon': 'assets/currency-listview-image-1.png',
    'title': "USD",
    'isSelected': true,
  },
  {
    'icon': 'assets/currency-listview-image-2.png',
    'title': "EUR",
    'isSelected': false,
  }
];
// End Currency Screen

// Top List View Items Screen
List topListViewItemsTitle = [
  'Curve & Plus',
  'The New New',
  'Everything',
  'Feeling Elegant',
  'Feeling Hot',
  'Feeling Dreamy',
  'Feeling Retro',
  'Feeling Cute',
];
// End Top List View Items Screen
