import '../../components/ListViewAppBar.dart';
import 'package:flutter/material.dart';
import '../../../../demo_data.dart';

class SettingScreen extends StatelessWidget {
  static const route = '/settings';

  const SettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('Settings'),
      backgroundColor: Colors.grey[100],
      body: Column(
        children: [
          BodyListView(),
          LogOutButton(),
        ],
      ),
    );
  }
}

class BodyListView extends StatelessWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16, bottom: 16),
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 2,
        itemBuilder: (context, index) => Container(
          padding: EdgeInsets.only(left: 16, top: 20, bottom: 20, right: 8),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(width: 1.5, color: Colors.grey[200]!))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                settings[index]['title'],
                style: TextStyle(fontSize: 16, color: Colors.grey[850]),
              ),
              Spacer(),
              Text(
                settings[index]['subTitle'],
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: Icon(
                  Icons.keyboard_arrow_right,
                  size: 16,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LogOutButton extends StatelessWidget {
  const LogOutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      color: Colors.white,
      child: TextButton(
          onPressed: () {},
          child: Text(
            'Log Out',
            style: TextStyle(
              color: Colors.redAccent[400],
              fontWeight: FontWeight.w900,
              fontSize: 16,
            ),
          )),
    );
  }
}
