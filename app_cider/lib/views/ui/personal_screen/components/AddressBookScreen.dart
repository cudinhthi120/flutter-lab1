import '../../components/BorderButton.dart';
import '../../components/ListViewAppBar.dart';
import 'package:flutter/material.dart';

class AddressBookScreen extends StatelessWidget {
  static const route = '/address-book';

  const AddressBookScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('Address'),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.only(top: 36),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8, top: 16),
              child: Image.asset(
                'assets/address-screen-top-image.png',
                fit: BoxFit.cover,
              ),
            ),
            Text(
              'It is empty here',
              style: TextStyle(color: Colors.grey),
            ),
            borderButton('Add Address'),
          ],
        ),
      ),
    );
  }
}
