
import 'package:flutter/material.dart';
import '../../../../demo_data.dart';
import '../../components/ListViewAppBar.dart';

class CurrencyScreen extends StatefulWidget {
  static const route = '/currency';
  const CurrencyScreen({Key? key}) : super(key: key);

  @override
  _CurrencyScreenState createState() => _CurrencyScreenState();
}

class _CurrencyScreenState extends State<CurrencyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: listViewAppBar('CurrencyScreen'),
      backgroundColor: Colors.grey[100],
      body: Container(
        margin: EdgeInsets.only(top: 16),
        color: Colors.white,
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: currency.length,
          itemBuilder: (context, index) => GestureDetector(
            onTap: () {
              setState(() {
                currency[index]['isSelected'] = !currency[index]['isSelected'];
              });
            },
            child: Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              padding: EdgeInsets.only(top: 8, bottom: 8, right: 8),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                  Border(bottom: BorderSide(width: 1, color: Colors.grey[200]!))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Image.asset(currency[index]['icon'], fit: BoxFit.cover,),
                  ),
                  Text(
                    currency[index]['title'],
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: currency[index]['isSelected'] == true ? FontWeight.bold : FontWeight.normal,
                        color: Colors.grey[850]),
                  ),
                  Spacer(),
                  SizedBox(
                    width: 15,
                    child: Transform.scale(
                      scale: 1,
                      child: Checkbox(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.zero,
                        ),
                        side: BorderSide.none,
                        checkColor: Colors.green,
                        fillColor: MaterialStateProperty.resolveWith((states) {
                          if (states.contains(MaterialState.selected)) {
                            return Colors.white;
                          }
                          return Colors.white;
                        }),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        value: currency[index]['isSelected'],
                        onChanged: (value) {
                          setState(() {
                            currency[index]['isSelected'] = value!;
                          });
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
