
import 'package:flutter/material.dart';

import '../../../../demo_data.dart';
import '../../components/ListViewItemsScreen.dart';

class PickAMood extends StatefulWidget {
  const PickAMood({Key? key}) : super(key: key);

  @override
  _PickAMoodState createState() => _PickAMoodState();
}

class _PickAMoodState extends State<PickAMood> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PickAMoodTitle(),
        PickAMoodGridView(),
      ],
    );
  }
}

class PickAMoodTitle extends StatelessWidget {
  const PickAMoodTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Text(
        'Pick A Mood',
        style: TextStyle(
          fontSize: 26,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class PickAMoodGridView extends StatefulWidget {
  const PickAMoodGridView({Key? key}) : super(key: key);

  @override
  _PickAMoodGridViewState createState() => _PickAMoodGridViewState();
}

class _PickAMoodGridViewState extends State<PickAMoodGridView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 450,
      child: GridView.builder(
        padding: EdgeInsets.only(left: 10, right: 10),
        physics: NeverScrollableScrollPhysics(),
        itemCount: 9,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 190,
        ),
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(TopListViewItemsScreen.route, arguments: pickAMood[index]['title']);
          },
          child: Container(
            margin: EdgeInsets.all(6),
            child: Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Image.asset(
                    pickAMood[index]['image'],
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    color: pickAMood[index]['color'],
                    child: Text(
                      pickAMood[index]['title'],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
