
import 'package:flutter/material.dart';

import '../../../../demo_data.dart';
import '../../components/ListViewItemsScreen.dart';

class ShopByCategory extends StatefulWidget {
  const ShopByCategory({Key? key}) : super(key: key);

  @override
  State<ShopByCategory> createState() => _ShopByCategoryState();
}

class _ShopByCategoryState extends State<ShopByCategory> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        ShopByCategoryTitle(),
        SizedBox(
          height: 10,
        ),
        ShopByCategoryContent(),
      ],
    );
  }
}

class ShopByCategoryTitle extends StatelessWidget {
  const ShopByCategoryTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Text(
        'Shop by Category',
        style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
      ),
    );
  }
}

class ShopByCategoryContent extends StatefulWidget {
  const ShopByCategoryContent({Key? key}) : super(key: key);

  @override
  State<ShopByCategoryContent> createState() => _ShopByCategoryContentState();
}

class _ShopByCategoryContentState extends State<ShopByCategoryContent> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(12),
      height: 400,
      width: double.infinity,
      child: Column(
        children: [
          Flexible(
            flex: 2,
            child: Row(
              children: [
                Flexible(
                  flex: 1,
                  child: Column(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed(
                                TopListViewItemsScreen.route,
                                arguments: 'Crop Tops & Camis');
                          },
                          child: Container(
                            margin: EdgeInsets.only(top: 8, right: 4, left: 4),
                            height: 90,
                            width: double.infinity,
                            child: Image.asset(
                              'assets/shop-by-category-left-img1.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed(
                                TopListViewItemsScreen.route,
                                arguments: 'Blouses & Shirts');
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                                top: 8, bottom: 8, right: 4, left: 4),
                            height: 90,
                            width: double.infinity,
                            child: Image.asset(
                              'assets/shop-by-category-left-img2.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                          TopListViewItemsScreen.route,
                          arguments: 'Dresses');
                    },
                    child: Container(
                      margin:
                          EdgeInsets.only(top: 8, bottom: 8, right: 4, left: 4),
                      height: double.infinity,
                      width: double.infinity,
                      child: Image.asset(
                        'assets/shop-by-category-right-img.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Flexible(
            flex: 1,
            child: ListView.builder(
              itemCount: shopByCategory.length,
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(TopListViewItemsScreen.route,
                      arguments: shopByCategoryTitle[index]);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8, right: 2, left: 4),
                  width: 100,
                  child: Image.asset(
                    shopByCategory[index],
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
