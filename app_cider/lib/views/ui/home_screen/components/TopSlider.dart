import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class TopSlider extends StatelessWidget {
  const TopSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.amber[100],
      ),
      child: CarouselSlider.builder(
        itemCount: 2,
        itemBuilder: (context, index, pageViewIndex) => Center(
          child: Text(
            '20% off for your first App order! Code in bag!',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.pinkAccent),
          ),
        ),
        options: CarouselOptions(
          height: 40,
          viewportFraction: 1,
          autoPlayAnimationDuration: Duration(milliseconds: 300),
          autoPlayInterval: Duration(seconds: 6),
          initialPage: 0,
          autoPlay: true,
        ),
      ),
    );
  }
}
