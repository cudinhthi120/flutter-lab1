import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../../demo_data.dart';
import '../../components/ListViewItemsScreen.dart';

class ShopByDrops extends StatefulWidget {
  const ShopByDrops({Key? key}) : super(key: key);

  @override
  _ShopByDropsState createState() => _ShopByDropsState();
}

class _ShopByDropsState extends State<ShopByDrops> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ShopByDropsTitle(),
        ShopByDropsListView(),
        ShopByDropsGetTheLook(),
      ],
    );
  }
}

class ShopByDropsTitle extends StatelessWidget {
  const ShopByDropsTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Shop by Drops',
            style: TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4, bottom: 4),
            child: Text(
              'Fun Fresh Must Have',
              style: TextStyle(
                fontSize: 12,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ShopByDropsListView extends StatefulWidget {
  const ShopByDropsListView({Key? key}) : super(key: key);

  @override
  _ShopByDropsListViewState createState() => _ShopByDropsListViewState();
}

class _ShopByDropsListViewState extends State<ShopByDropsListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 380,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 12, right: 12),
        itemCount: shopByDrops.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(TopListViewItemsScreen.route, arguments: shopByDrops[index]['title']);
          },
          child: Container(
            margin: EdgeInsets.only(right: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(shopByDrops[index]['image']),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Row(children: [
                    Text(shopByDrops[index]['title'], style: TextStyle(fontWeight: FontWeight.bold),),
                    Padding(
                      padding: const EdgeInsets.only(left: 2.0),
                      child: Image.asset(shopByDrops[index]['icon']),
                    ),
                  ],),
                ),
                Text(shopByDrops[index]['subTitle'], style: TextStyle(color: Colors.grey[600], fontSize: 12),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ShopByDropsGetTheLook extends StatefulWidget {
  const ShopByDropsGetTheLook({Key? key}) : super(key: key);

  @override
  _ShopByDropsGetTheLookState createState() => _ShopByDropsGetTheLookState();
}

class _ShopByDropsGetTheLookState extends State<ShopByDropsGetTheLook> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      width: double.infinity,
      child: Image.asset('assets/shop-by-drop-content-img.png', fit: BoxFit.cover,),
    );
  }
}
