import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ImageSlider extends StatelessWidget {
  const ImageSlider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CarouselSlider.builder(
        itemCount: 2,
        options: CarouselOptions(
          height: 550,
          autoPlay: true,
          initialPage: 0,
          viewportFraction: 1,
          autoPlayAnimationDuration: Duration(milliseconds: 500),
        ),
        itemBuilder: (context, index, pageViewIndex) => Image.asset(
          'assets/slider-image1.png',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}