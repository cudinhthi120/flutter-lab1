import '../../../../demo_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../components/ProductScreen.dart';

class DailyFlashSale extends StatefulWidget {
  const DailyFlashSale({Key? key}) : super(key: key);

  @override
  _DailyFlashSaleState createState() => _DailyFlashSaleState();
}

class _DailyFlashSaleState extends State<DailyFlashSale> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DailyFlashSaleTitle(),
        DailyFlashSaleListView(),
      ],
    );
  }
}

class DailyFlashSaleTitle extends StatelessWidget {
  const DailyFlashSaleTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Text(
            'Daily Flash Sale',
            style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
          ),
          Image.asset('assets/daily-flash-sale-title-img.png'),
          Spacer(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.remove_red_eye_outlined,
              color: Colors.black54,
            ),
          ),
          Text(
            'See all',
            style: TextStyle(color: Colors.black54),
          ),
        ],
      ),
    );
  }
}

class DailyFlashSaleListView extends StatefulWidget {
  const DailyFlashSaleListView({Key? key}) : super(key: key);

  @override
  _DailyFlashSaleListViewState createState() => _DailyFlashSaleListViewState();
}

class _DailyFlashSaleListViewState extends State<DailyFlashSaleListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(left: 8, right: 8),
        itemCount: dailyFlashSale.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(ProductScreen.route, arguments: dailyFlashSale[index]['name']);
          },
          child: Container(
            width: 110,
            margin: EdgeInsets.only(left: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                    height: 150,
                    child: Image.asset(
                     dailyFlashSale[index]['image'],
                      fit: BoxFit.cover,
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    dailyFlashSale[index]['name'],
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 4),
                  child: Text(
                    dailyFlashSale[index]['newPrice'],
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.deepOrangeAccent,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ),
                Text(
                  dailyFlashSale[index]['oldPrice'],
                  style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                      decoration: TextDecoration.lineThrough),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
