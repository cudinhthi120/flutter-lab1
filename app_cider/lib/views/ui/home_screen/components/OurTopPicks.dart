import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../components/GirdViewWithListViewBelow.dart';

class OurTopPicks extends StatefulWidget {
  const OurTopPicks({Key? key}) : super(key: key);

  @override
  _OurTopPicksState createState() => _OurTopPicksState();
}

class _OurTopPicksState extends State<OurTopPicks> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        OurTopPicksTitle(),
        OurTopPicksGridView(),
      ],
    );
  }
}

class OurTopPicksTitle extends StatelessWidget {
  const OurTopPicksTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16, top: 50, bottom: 16),
      child: Text(
        'Our Top Picks',
        style: TextStyle(
          fontSize: 26,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class OurTopPicksGridView extends StatefulWidget {
  const OurTopPicksGridView({Key? key}) : super(key: key);

  @override
  _OurTopPicksGridViewState createState() => _OurTopPicksGridViewState();
}

class _OurTopPicksGridViewState extends State<OurTopPicksGridView> {
  @override
  Widget build(BuildContext context) {
    return GridViewWithListViewBelow();
  }
}
