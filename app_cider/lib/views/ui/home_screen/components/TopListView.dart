import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../../demo_data.dart';
import '../../components/ListViewItemsScreen.dart';

class TopListView extends StatefulWidget {
  const TopListView({Key? key}) : super(key: key);

  @override
  _TopListViewState createState() => _TopListViewState();
}

class _TopListViewState extends State<TopListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 90,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 12, right: 12),
        scrollDirection: Axis.horizontal,
        itemCount: topListView.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(TopListViewItemsScreen.route, arguments: topListViewItemsTitle[index]);
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: topListView[index]['image'],
                ),
                margin: EdgeInsets.only(left: 8, right: 8, top: 12, bottom: 4),
                height: 50,
                width: 50,
              ),
              Container(
                width: 45,
                  margin: EdgeInsets.only(left: 8, right: 8),
                  child: Text(
                    topListView[index]['title'],
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
