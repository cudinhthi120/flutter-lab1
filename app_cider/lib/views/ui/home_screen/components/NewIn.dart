import 'package:cloud_firestore/cloud_firestore.dart';
import '../../../../demo_data.dart';
import '../../components/ProductScreen.dart';
import 'package:flutter/material.dart';

class NewIn extends StatelessWidget {
  const NewIn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NewInTitle(),
        NewInImageListView(),
      ],
    );
  }
}

class NewInTitle extends StatelessWidget {
  const NewInTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'New In',
                style: TextStyle(
                  fontSize: 26,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Image.asset('assets/new-in-title-img.png'),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.remove_red_eye_outlined,
                  color: Colors.black54,
                ),
              ),
              Text(
                'See All',
                style: TextStyle(
                  color: Colors.black54,
                ),
              ),
            ],
          ),
          Text(
            'Fun Fresh Must Have',
            style: TextStyle(
              fontSize: 12,
              color: Colors.black54,
            ),
          ),
        ],
      ),
    );
  }
}

class NewInImageListView extends StatefulWidget {
  const NewInImageListView({Key? key}) : super(key: key);

  @override
  _NewInImageListViewState createState() => _NewInImageListViewState();
}

class _NewInImageListViewState extends State<NewInImageListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 8, right: 8),
        scrollDirection: Axis.horizontal,
        itemCount: newIn.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(ProductScreen.route,
                arguments: newIn[index]['name']);
          },
          child: Container(
            width: 110,
            margin: EdgeInsets.only(left: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 150,
                  child: Image.asset(
                    newIn[index]['image'],
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    newIn[index]['name'],
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4, bottom: 4),
                  child: Text(
                    newIn[index]['price'],
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ),
                Text(
                  newIn[index]['color'],
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.grey[600],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
