import '../search_screen/SearchScreen.dart';
import '../shopping_cart/ShoppingCart.dart';
import 'components/DailyFlashSale.dart';
import 'components/Inspiration.dart';
import 'components/OurTopPicks.dart';
import 'components/PickAMood.dart';
import 'package:flutter/material.dart';
import 'components/ImageSlider.dart';
import 'components/NewIn.dart';
import 'components/ShopByCategory.dart';
import 'components/ShopByDrops.dart';
import 'components/TopListView.dart';
import 'components/TopSlider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        title: Text(
          'Cinder'.toUpperCase(),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(SearchScreen.route);
              },
              child: Icon(
                Icons.search,
                size: 22,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(0.5),
          child: Container(
            color: Colors.grey[300],
            height: 0.5,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TopSlider(),
            TopListView(),
            ImageSlider(),
            ShopByCategory(),
            NewIn(),
            DailyFlashSale(),
            Inspiration(),
            ShopByDrops(),
            PickAMood(),
            OurTopPicks(),
          ],
        ),
      ),
    );
  }
}

