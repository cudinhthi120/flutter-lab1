import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../components/BorderButton.dart';
import '../shopping_cart/ShoppingCart.dart';

class ProductScreen extends StatelessWidget {
  static const route = '/product';

  const ProductScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = ModalRoute.of(context)!.settings.arguments as String;
    double height = MediaQuery.of(context).size.height - 190;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        actions: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1),
          child: Container(
            color: Colors.grey[300],
            height: 1,
          ),
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.amber[100],
                    ),
                    child: CarouselSlider.builder(
                      itemCount: 2,
                      itemBuilder: (context, index, pageViewIndex) => Center(
                        child: Text(
                          '20% off for your first App order! Code in bag!',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: Colors.pinkAccent),
                        ),
                      ),
                      options: CarouselOptions(
                        height: 40,
                        viewportFraction: 1,
                        autoPlayAnimationDuration: Duration(milliseconds: 300),
                        autoPlayInterval: Duration(seconds: 6),
                        initialPage: 0,
                        autoPlay: true,
                      ),
                    ),
                  ),
                  Container(
                    height: height,
                    color: Colors.orangeAccent,
                    child: Image.asset('assets/daily-flash-sale-listview-items-1.png', fit: BoxFit.cover,),
                  ),
                  Container(
                    height: 1000,
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.white,
              child: borderButton('Add to Bag'.toUpperCase()),
            ),
          ),
        ],
      ),
    );
  }
}
