import 'package:flutter/material.dart';

PreferredSizeWidget listViewAppBar (title) {
  return AppBar(
    elevation: 0,
    title: Text(
      title,
      style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16),
    ),
    centerTitle: true,
    backgroundColor: Colors.white,
    foregroundColor: Colors.black,
    bottom: PreferredSize(
      preferredSize: Size.fromHeight(1),
      child: Container(
        color: Colors.grey[300],
        height: 1,
      ),
    ),
  );
}