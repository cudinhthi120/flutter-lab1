import 'package:flutter/material.dart';

class TopContent extends StatelessWidget {
  const TopContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/wishlist-top-img.png', fit: BoxFit.cover,),
          ),
          Text('Your Wishlist is currently empty', style: TextStyle(color: Colors.grey),),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.only(top: 100.0, left: 32, right: 32),
            child: Image.asset('assets/wishlist-body-img.png', fit: BoxFit.fill,),
          ),
        ],
      ),
    );
  }
}
