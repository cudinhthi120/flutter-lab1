import 'package:flutter/material.dart';
import '../shopping_cart/ShoppingCart.dart';
import 'components/TopContent.dart';
import 'components/YouMayAlsoLike.dart';
import '../search_screen/SearchScreen.dart';

class WishListScreen extends StatefulWidget {
  const WishListScreen({Key? key}) : super(key: key);

  @override
  _WishListScreenState createState() => _WishListScreenState();
}

class _WishListScreenState extends State<WishListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Wishlist',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(SearchScreen.route);
              },
              child: Icon(
                Icons.search,
                size: 22,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ShoppingCart.route);
              },
              child: Icon(
                Icons.shopping_bag_outlined,
                size: 22,
              ),
            ),
          ),
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(0.5),
          child: Container(
            color: Colors.grey[300],
            height: 0.5,
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            TopContent(),
            YouMayAlsoLike(),
          ],
        ),
      ),
    );
  }
}
