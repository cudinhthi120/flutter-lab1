import 'package:flutter/material.dart';

import '../../../demo_data.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool isChecked = true;

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          SignUpEmailField(),
          SignUpPasswordField(),
          ConfirmButton(),
          TextAndCheckBox(),
          LineBreak(),
          OtherSignUpMethod()
        ],
      ),
    );
  }

  Widget SignUpEmailField() {
    return Container(
      padding: const EdgeInsets.only(top: 12, bottom: 12),
      height: 70,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        cursorWidth: 1,
        maxLines: 1,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 12, right: 12),
          hintText: 'Email',
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
            borderRadius: BorderRadius.zero,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget SignUpPasswordField() {
    return Container(
      padding: const EdgeInsets.only(top: 12, bottom: 12),
      height: 70,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        cursorWidth: 1,
        maxLines: 1,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 12, right: 12),
          hintText: 'At least 6 characters',
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
            borderRadius: BorderRadius.zero,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget ConfirmButton() {
    return Container(
      padding: EdgeInsets.only(top: 16),
      width: double.infinity,
      child: TextButton(
        style: TextButton.styleFrom(
            primary: Colors.black,
            backgroundColor: Colors.amber[100],
            side: BorderSide(
              width: 2,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            )),
        onPressed: () {},
        child: Align(
          alignment: Alignment.center,
          child: Text(
            'Register NOW! 20% OFF your order!',
            style: TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  Widget LineBreak() {
    return Padding(
      padding: EdgeInsets.only(top: 18, bottom: 18),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0.5,
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 8, right: 8),
            child: Text(
              'Or',
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
          Expanded(
            child: Container(height: 0.5, color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget TextAndCheckBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'By registering, I agree with Cider s ',

                style: TextStyle(fontSize: 12, color: Colors.grey,),
              ),
              Text(
                'Privacy Policy',
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.black,

                    decoration: TextDecoration.underline,),
              ),
            ],
          ),
          Row(
            children: [
              SizedBox(
                width: 14,
                child: Transform.scale(
                  scale: 0.7,
                  child: Checkbox(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                    side: BorderSide(
                      width: 1.8,
                    ),
                    checkColor: Colors.red,
                    fillColor: MaterialStateProperty.resolveWith((states) {
                      if (states.contains(MaterialState.selected)) {
                        return Colors.red[100];
                      }
                      return Colors.black;
                    }),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Text(
                  'Stay up-to-date on exclusive offers and latest news by email',
                  style: TextStyle(fontSize: 12, color: Colors.grey),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget OtherSignUpMethod() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      height: 120,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 2,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(width: 1, color: Colors.grey[350]!)
                ),
                child: Image.asset(otherLoginMethod[index]['icon'],),),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  otherLoginMethod[index]['text'],
                  style: TextStyle(color: Colors.grey, fontSize: 10),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
