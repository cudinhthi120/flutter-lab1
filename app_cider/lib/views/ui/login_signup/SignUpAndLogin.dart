import 'LoginScreen.dart';
import 'SignUpScreen.dart';
import 'package:flutter/material.dart';

class SignUpAndLogin extends StatefulWidget {
  const SignUpAndLogin({Key? key}) : super(key: key);

  @override
  _SignUpAndLoginState createState() => _SignUpAndLoginState();
}

class _SignUpAndLoginState extends State<SignUpAndLogin> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16, left: 32, right: 32),
      child: DefaultTabController(
        initialIndex: 1,
        length: 2,
        child: Column(
          children: [
            TabBar(
              labelPadding: EdgeInsets.only(bottom: 8),
              labelColor: Colors.black,
              unselectedLabelColor: Colors.grey,
              indicatorColor: Colors.black,
              indicatorWeight: 1,
              tabs: [
                Text('Log in'),
                Text('Register'),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              height: 500,
              child: TabBarView(
                children: [
                  LoginScreen(),
                  SignUpScreen(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
