import 'package:flutter/material.dart';
import '../../../demo_data.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          LoginEmailField(),
          LoginPasswordField(),
          ForgotPassword(),
          ConfirmButton(),
          LineBreak(),
          OtherLoginMethod(),
        ],
      ),
    );
  }

  Widget LoginEmailField() {
    return Container(
      padding: const EdgeInsets.only(top: 12, bottom: 12),
      height: 70,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        cursorWidth: 1,
        maxLines: 1,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 12, right: 12),
          hintText: 'Email',
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
            borderRadius: BorderRadius.zero,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget LoginPasswordField() {
    return Container(
      padding: const EdgeInsets.only(top: 12, bottom: 12),
      height: 70,
      child: TextFormField(
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        textAlignVertical: TextAlignVertical.center,
        cursorWidth: 1,
        maxLines: 1,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.only(left: 12, right: 12),
          hintText: 'Password',
          hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
            borderRadius: BorderRadius.zero,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.zero,
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
          suffixIcon: Icon(Icons.remove_red_eye_outlined),
          suffixIconColor: Colors.grey,
        ),
      ),
    );
  }

  Widget ForgotPassword() {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(
          'Forgot your password?',
          style: TextStyle(
            color: Colors.black,
            decoration: TextDecoration.underline,
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  Widget ConfirmButton() {
    return Container(
      padding: EdgeInsets.only(top: 16),
      width: double.infinity,
      child: TextButton(
        style: TextButton.styleFrom(
            primary: Colors.black,
            backgroundColor: Colors.amber[100],
            side: BorderSide(
              width: 2,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            )),
        onPressed: () {},
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 6),
                child: Icon(
                  Icons.lock,
                  size: 20,
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                'Log in',
                style: TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget LineBreak() {
    return Padding(
      padding: EdgeInsets.only(top: 18, bottom: 18),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 0.5,
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 8, right: 8),
            child: Text(
              'Or',
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
          Expanded(
            child: Container(height: 0.5, color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget OtherLoginMethod() {
    return Container(
      padding: EdgeInsets.only(top: 12),
      height: 120,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: 2,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 45,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(width: 1, color: Colors.grey[350]!)
                ),
                  child: Image.asset(otherLoginMethod[index]['icon'],),),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Text(
                  otherLoginMethod[index]['text'],
                  style: TextStyle(color: Colors.grey, fontSize: 10),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
