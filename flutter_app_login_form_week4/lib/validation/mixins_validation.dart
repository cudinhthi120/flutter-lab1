mixin CommonValidation {
  String? validateEmail(String? value) {
    if (value!.isEmpty) {
      return 'please enter your Email';
    }
    if (!value.contains('@')) {
      return 'please input valid Email.';
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value!.isEmpty) {
      return 'please input your password';
    }
    if (value.length < 8 ||
        !value.contains(RegExp(r'[A-Z]')) ||
        !value.contains(RegExp(r'[a-z]')) ||
        !value.contains(RegExp(r'[!@#£$%^&*(),.?":{}~<>]'))) {
      return 'Password must be at least 8 characters including\n'
          'One lower case letter\n'
          'One uppercase letter\n'
          'One special character.';
    }
    // if (!value.contains(RegExp(r'[A-Z]')) || !value.contains(RegExp(r'[a-z]'))) {
    //   return 'Password must have one lower case, one uppercase letter.';
    // }
    // if (!value.contains(RegExp(r'[!@#£$%^&*(),.?":{}~<>]'))) {
    //   return 'Password must have one special character.';
    // }
    return null;
  }
}
