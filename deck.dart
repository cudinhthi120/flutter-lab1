void main() {
  var deck = Deck();
  print(deck);
}

class Deck {
  List<Card> cards = [];

  Deck() {
    var ranks = ['Ace', 'Two', 'Three', 'Four', 'Five'];
    var suits = ['Diamonds', 'Hearts', 'Clubs', 'Spades'];

    for (var rank in ranks) {
      for (var suit in suits) {
        cards.add(Card(ranks: rank, suits: suit));
      }
    }
  }

  @override
  String toString() {
    return cards.toString();
  }
}

class Card {
  String? suits;
  String? ranks;

  Card({this.suits, this.ranks});

  @override
  String toString() {
    return '$ranks of $suits';
  }
}
