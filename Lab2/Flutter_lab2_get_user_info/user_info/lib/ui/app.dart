import 'package:flutter/material.dart';
import 'package:user_info/validation/validation.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override 
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Get user info",
      home: Scaffold(
        appBar:  AppBar(title: Text('Get user info')),
        body: Container(
          padding: EdgeInsets.all(24.0),
          child: GetInfoScreen(),
        ),
      ),
    );
  }
}

class GetInfoScreen extends StatefulWidget {
  const GetInfoScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return InfoState();
  }
}

class InfoState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;
  late String firstName;
  late String lastName;
  late num birth;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
          key: formKey,
          child: Column(
            children: [
              fieldFirstName(),
              Container(margin: EdgeInsets.only(top: 20.0),),
              fieldLastName(),
              Container(margin: EdgeInsets.only(top: 40.0),),
              fieldEmailAddress(),
              Container(margin:  EdgeInsets.only(top: 40.0),),
              fieldBirth(),
              Container(margin: EdgeInsets.only(top: 40.0),),
              fieldAddress(),
              Container(margin:  EdgeInsets.only(top: 40.0),),
              confirmButton(),
            ],
          ),
        )
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email_outlined),
          labelText: 'Email address'
      ),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldLastName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Last Name'
      ),
      validator: validateLastName,
      onSaved: (value) {
        firstName = value as String;
      },
    );
  }

  Widget fieldFirstName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'First Name'
      ),
      validator: validateFirstName,
      onSaved: (value) {
        lastName = value as String;
      },
    );
  }

  Widget fieldBirth() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          icon: Icon(Icons.calendar_today_outlined),
          labelText: "Birth"
      ),
      validator: validateBirth,
      onSaved:  (value) {
        birth = value as num;
      },
    );
  }

  Widget fieldAddress() {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        icon: Icon(Icons.home),
        labelText: "Address"
      ),
      validator:  validateAddress,
      onSaved:  (value) {
        address = value as String;
      },
    );
  }

  Widget confirmButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $firstName, $lastName, $birth, $address ');
          }
        },
        child: Text('confirm')
    );
  }
}
