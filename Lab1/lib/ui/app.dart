import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../model/image_model.dart';
import 'dart:convert';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }

}

class AppState extends State<App> {
  int counter = 0;
  List<ImageModel> imagesList = [];

  fetchImages() async {
    counter++;
    var url = Uri.https('jsonplaceholder.typicode.com', 'photos/$counter');
    var response = await http.get(url);

    print(response.body);

    var jsonObject = json.decode(response.body);
    var imageModel = ImageModel(jsonObject['id'], jsonObject['url']);
    imagesList.add(imageModel);


    print('counter=$counter');
    print('Length of images=${imagesList.length}');

    setState(() {
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var appWidget = new MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Image Viewer'),),
          body: ListView.builder(
              itemCount: imagesList.length,
              itemBuilder: (context, index){
                return ListTile(
                  title: Image.network(imagesList[index].url),
                );
              }
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: fetchImages
          ),
        )
    );

    return appWidget;
  }
  
}