import 'package:flutter/material.dart';
import '../model/image_model.dart';
import '../stream/image_stream.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  ImageLinkStream imageStream = ImageLinkStream();
  List<ImageModel> imageList = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab3-2',
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Lab3-2"),
        ),
        body: ListView.builder(
          itemCount: imageList.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Image.network(imageList[index].url ?? ""),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('Press!'),
          onPressed: () {
            changeImage();
          },
        ),
      ),
    );
  }

  changeImage() async {
    imageStream.getImageLink().listen((eventImageLink) {
      setState(() {
        print(eventImageLink);
        imageList.add(eventImageLink!);
      });
    });
  }
}