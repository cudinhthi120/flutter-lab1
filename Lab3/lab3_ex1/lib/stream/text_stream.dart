import 'package:flutter/material.dart';
import 'dart:math';

class TextStream {

  Stream<String?> getText() async* {
    final List<String> listMessage = [
      'Hôm nay là thứ 2',
      'Hôm nay là thứ 3',
      'Hôm nay là thứ 4',
      'Hôm nay là thứ 5',
      'Hôm nay là thứ 6',
      'Hôm nay là thứ 7',
      'Hôm nay là thứ CN',
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      // int index = t % 7;
      // return listMessage[index];

      // Random Message
      final _random = Random();
      return listMessage[_random.nextInt(listMessage.length)];
    });
  }
}
