import 'package:flutter/material.dart';
import '../stream/text_stream.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  TextStream textStream = TextStream();
  List<String> textList = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 3-1',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Lab3-1'),
        ),
        body: ListView.builder(
          itemCount: textList.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(textList[index]),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: const Text('Press!'),
          onPressed: () {
            changeText();
          },
        ),
      ),
    );
  }

  changeText() async {
    textStream.getText().listen((eventText) {
      setState(() {
        print(eventText);
        textList.add(eventText!);
      });
    });
  }
}
