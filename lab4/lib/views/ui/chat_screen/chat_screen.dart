import 'package:flutter/material.dart';
import 'box_chat_screen.dart';
import '../../../data/demo_data.dart';

class ChatScreen extends StatefulWidget {
  static const route = '/chat';
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}
class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    String title = 'Chat';
    return Scaffold(
      appBar: AppBar(
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 18,
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(title),
      ),
      body: Center(
        child: Column(
          children: const [
            SearchField(),
            BodyListView(),
          ],
        ),
      ),
    );
  }
}

class SearchField extends StatelessWidget {
  const SearchField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
      height: 65,
      decoration: BoxDecoration(
        border: Border.symmetric(horizontal: BorderSide(color: Colors.grey[300]!) ),
        color: Colors.white,
      ),
      child: const TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        maxLines: 1,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.zero,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.grey),
          ),
          hintText: 'Tìm theo người dùng...',
          hintStyle: TextStyle(color: Colors.grey),
          prefixIcon: Icon(Icons.search),
        ),
      ),
    );
  }
}

class BodyListView extends StatelessWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String buttonName = 'Chat';
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: titleList.length,
      itemBuilder: (context, index) => Container(
        decoration: const BoxDecoration(
          border: Border.symmetric(horizontal: BorderSide(color: Colors.black, width: 0.2))
        ),
        child: Card(
          margin: EdgeInsets.only(bottom: 1),
          elevation: 0,
          child: ListTile(
            title: Text(titleList[index], maxLines: 1, overflow: TextOverflow.ellipsis,),
            subtitle: Text(subTitleList[index], maxLines: 1, overflow: TextOverflow.ellipsis,),
            leading: Image.asset(imgList[index], width: 60,),
            trailing: TextButton(
              child: Text(buttonName),
              style: TextButton.styleFrom(
                minimumSize: const Size(90, 30),
                primary: Colors.white,
                backgroundColor: Colors.red,
              ),
              onPressed: () {
                var title = titleList[index];
                Navigator.of(context).pushNamed(BoxChatScreen.route, arguments: title);
              },
            ),
          ),
        ),
      ),
    );
  }
}
