import 'package:flutter/material.dart';
import '../../../data/demo_data.dart';

class BoxChatScreen extends StatefulWidget {
  static const route = '/box-chat';

  const BoxChatScreen({Key? key}) : super(key: key);

  @override
  _BoxChatScreenState createState() => _BoxChatScreenState();
}

class _BoxChatScreenState extends State<BoxChatScreen> {
  @override
  Widget build(BuildContext context) {
    String title = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
        foregroundColor: Colors.black,
        title: Text(title),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(0.3),
          child: Container(
            height: 0.3,
            color: Colors.grey,
          ),
        ),
      ),
      body: Stack(
        children: [
          ChatField(),
          InputField(),
        ],
      ),
    );
  }
}

class ChatField extends StatefulWidget {
  const ChatField({Key? key}) : super(key: key);

  @override
  _ChatFieldState createState() => _ChatFieldState();
}

class _ChatFieldState extends State<ChatField> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: messages.length,
      itemBuilder: (context, index) => Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
        child: Align(
          alignment: (messages[index].messageType == 'receiver'
              ? Alignment.topLeft
              : Alignment.topRight),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: 300,
            ),
            child: Container(
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  borderRadius: (messages[index].messageType == 'receiver'
                      ? const BorderRadius.only(
                          topRight: Radius.circular(20),
                          topLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20))
                      : const BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                          bottomLeft: Radius.circular(20))
                  ),
                  color: (messages[index].messageType == 'receiver'
                      ? Colors.grey[300]
                      : Colors.blue[300])),
              child: Text(
                messages[index].messageContent,
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class InputField extends StatefulWidget {
  const InputField({Key? key}) : super(key: key);

  @override
  _InputFieldState createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        padding: EdgeInsets.only(left: 10, bottom: 10, top: 10),
        decoration: BoxDecoration(
          border: Border.symmetric(
            horizontal: BorderSide(color: Colors.grey, width: 0.5),
          ),
          color: Colors.white,
        ),
        height: 60,
        width: double.infinity,
        child: Row(
          children: [
            Icon(Icons.photo, color: Colors.blue[600],),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: TextField(
                  keyboardAppearance: Brightness.light,
                  cursorWidth: 1,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                    hintText: 'Nhập nội dung chat...',
                    contentPadding:
                        EdgeInsets.only(left: 16),
                    filled: true,
                    fillColor: Colors.white,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.circular(20)
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.grey),
                        borderRadius: BorderRadius.circular(20)
                    ),
                    suffixIcon: Icon(
                      Icons.send_sharp,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
