import 'package:flutter/material.dart';

class CartScreen extends StatelessWidget {
  static const route = '/cart';

  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text('Giỏ hàng'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 12),
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 200),
        child: Column(
          children: [
            Text('Bạn chưa có sản phẩm nào trong giỏ hàng'),
            Container(
              margin: EdgeInsets.only(top: 30),
              height: 45,
              width: double.infinity,
              child: TextButton(
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  backgroundColor: Colors.red,
                  textStyle: TextStyle(
                    fontSize: 17,
                  )
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'Tiếp tục mua sắm'.toUpperCase(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
