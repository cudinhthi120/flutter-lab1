import 'package:flutter/material.dart';
import '../personal_screen/personal_screen.dart';
import 'login_page.dart';
import 'sign_up_page.dart';

class LoginAndSignUpScreen extends StatefulWidget {
  static const route = '/login';
  const LoginAndSignUpScreen({Key? key}) : super(key: key);

  @override
  _LoginAndSignUpScreenState createState() => _LoginAndSignUpScreenState();
}

class _LoginAndSignUpScreenState extends State<LoginAndSignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: TextButton(
            onPressed: () {
              Navigator.of(context).pop(PersonalScreen.route);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          title: Text('Đăng nhập / Đăng ký'),
          bottom: PreferredSize(
            preferredSize: Size(50, 50),
            child: AppBarTabBar(),
          ),
        ),
        body: TabBarView(
          children: [
            LoginPage(),
            SignUpPage(),
          ],
        ),
      ),
    );
  }
}

class AppBarTabBar extends StatelessWidget {
  const AppBarTabBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      width: double.infinity,
      child: TabBar(
        labelStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
        labelColor: Colors.black,
        unselectedLabelColor: Colors.black54,
        indicatorColor: Colors.yellow[700],
        tabs: [
          Tab(
            child: Text('Đăng nhập'.toUpperCase()),
          ),
          Tab(
            child: Text('Đăng ký'.toUpperCase()),
          ),
        ],
      ),
    );
  }
}

