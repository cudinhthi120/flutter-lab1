import 'package:flutter/material.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Form(
      key: formKey,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              children: const [
                NameInputField(),
                PhoneInputField(),
                EmailInputField(),
                PasswordInputField(),
                BirthInputField(),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Gender(),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 14, vertical: 10),
            child: SignUpButton(),
          ),
          BottomText(),
        ],
      ),
    );
  }
}

class NameInputField extends StatelessWidget {
  const NameInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
        labelText: 'Họ tên',
        labelStyle: TextStyle(color: Colors.black, fontSize: 17),
      ),
    );
  }
}

class PhoneInputField extends StatelessWidget {
  const PhoneInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        labelText: 'Số điện thoại',
        labelStyle: TextStyle(color: Colors.black, fontSize: 17),
      ),
    );
  }
}

class EmailInputField extends StatelessWidget {
  const EmailInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email',
        labelStyle: TextStyle(color: Colors.black, fontSize: 17),
      ),
    );
  }
}

class PasswordInputField extends StatelessWidget {
  const PasswordInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.visiblePassword,
      decoration: InputDecoration(
        labelText: 'Mật khẩu',
        labelStyle: TextStyle(color: Colors.black, fontSize: 17),
      ),
    );
  }
}

class BirthInputField extends StatelessWidget {
  const BirthInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'Ngày sinh',
        labelStyle: TextStyle(
          color: Colors.black,
          fontSize: 17,
        ),
      ),
    );
  }
}

class Gender extends StatefulWidget {
  const Gender({Key? key}) : super(key: key);

  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  String _selectedGender = 'gender';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Radio<String>(
                value: 'male',
                groupValue: _selectedGender,
                onChanged: (value) {
                  setState(() {
                    _selectedGender = value!;
                  });
                },
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                activeColor: Colors.blue,
              ),
              Text(
                'Nam',
                style: TextStyle(fontSize: 17),
              ),
            ],
          ),
          Row(
            children: [
              Radio<String>(
                value: 'female',
                groupValue: _selectedGender,
                onChanged: (value) {
                  setState(() {
                    _selectedGender = value!;
                  });
                },
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                activeColor: Colors.blue,
              ),
              Text(
                'Nữ',
                style: TextStyle(fontSize: 17),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SignUpButton extends StatelessWidget {
  const SignUpButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 45,
      margin: EdgeInsets.only(bottom: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: Colors.red,
          primary: Colors.white,
          textStyle: TextStyle(
            fontSize: 17,
          ),
        ),
        onPressed: () {},
        child: Text(
          'Đăng Ký'.toUpperCase(),
        ),
      ),
    );
  }
}

class BottomText extends StatelessWidget {
  const BottomText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12),
      child: Text(
        'Khi đăng ký là bạn đã chấp nhận điều khoản sử dụng',
        style: TextStyle(
          fontSize: 13,
          color: Colors.grey[700],
        ),
      ),
    );
  }
}
