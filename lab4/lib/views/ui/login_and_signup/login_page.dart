import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return Container(
      padding: EdgeInsets.all(15),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            EmailInputField(),
            PasswordInputField(),
            LoginButton(),
            MiddleText(),
            OtherLoginMethod(),
          ],
        ),
      ),
    );
  }
}

class EmailInputField extends StatelessWidget {
  const EmailInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email/Số điện thoại',
        labelStyle: TextStyle(
          color: Colors.black,
          fontSize: 17,
        ),
      ),
    );
  }
}

class PasswordInputField extends StatelessWidget {
  const PasswordInputField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Mật khẩu',
        labelStyle: TextStyle(
          color: Colors.black,
          fontSize: 17,
        ),
      ),
    );
  }
}

class LoginButton extends StatelessWidget {
  const LoginButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 45,
      margin: EdgeInsets.only(top: 55, bottom: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: Colors.red,
          primary: Colors.white,
          textStyle: TextStyle(
            fontSize: 17,
          ),
        ),
        onPressed: () {},
        child: Text(
          'Đăng nhập'.toUpperCase(),
        ),
      ),
    );
  }
}

class MiddleText extends StatelessWidget {
  const MiddleText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: TextStyle(color: Colors.grey[700]),
      child: Column(
        children: const [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text('Khi đăng nhập là bạn đã chấp nhận điều khoản sử dụng'),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              'Quên mật khẩu?',
              style: TextStyle(
                color: Colors.blue,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text('Hoặc đăng nhập với'),
          ),
        ],
      ),
    );
  }
}

class OtherLoginMethod extends StatelessWidget {
  const OtherLoginMethod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              color: Colors.blue[900],
              margin: const EdgeInsets.only(right: 1.0),
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'Facebook',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.blue,
              margin: const EdgeInsets.only(right: 1.0),
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'Zalo',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.green,
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'Gmail',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
