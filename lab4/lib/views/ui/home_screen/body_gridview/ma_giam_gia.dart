import 'package:flutter/material.dart';

class DiscountCode extends StatefulWidget {
  static const route = '/discount-code';

  const DiscountCode({Key? key}) : super(key: key);

  @override
  _DiscountCodeState createState() => _DiscountCodeState();
}

class _DiscountCodeState extends State<DiscountCode> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: AppBarTextField(),
        actions: [
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: (){},
          ),
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.more_horiz),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

class AppBarTextField extends StatefulWidget {
  const AppBarTextField({Key? key}) : super(key: key);

  @override
  _AppBarTextFieldState createState() => _AppBarTextFieldState();
}

class _AppBarTextFieldState extends State<AppBarTextField> {
  @override
  Widget build(BuildContext context) {
    String discountHintText = ModalRoute.of(context)!.settings.arguments as String;
    return Container(
      alignment: Alignment.center,
      height: 40,
      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        cursorHeight: 20,
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(right: 12),
            filled: true,
            fillColor: Colors.white,
            hintText: discountHintText,
            prefixIcon: Icon(Icons.search),
            prefixIconColor: Colors.grey,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.zero),
        ),
      ),
    );
  }
}
