import 'package:flutter/material.dart';

class DepartmentStoreScreen extends StatefulWidget {
  static const route = '/department-store';

  const DepartmentStoreScreen({Key? key}) : super(key: key);

  @override
  _DepartmentStoreScreenState createState() => _DepartmentStoreScreenState();
}

class _DepartmentStoreScreenState extends State<DepartmentStoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: AppBarTextField(),
        actions: [
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: (){},
          ),
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.more_horiz),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

class AppBarTextField extends StatefulWidget {
  const AppBarTextField({Key? key}) : super(key: key);

  @override
  _AppBarTextFieldState createState() => _AppBarTextFieldState();
}

class _AppBarTextFieldState extends State<AppBarTextField> {
  @override
  Widget build(BuildContext context) {
    String hintText = ModalRoute.of(context)!.settings.arguments as String;
    return Container(
      alignment: Alignment.center,
      height: 40,
      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        cursorHeight: 20,
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(right: 12),
            filled: true,
            fillColor: Colors.white,
            hintText: hintText,
            prefixIcon: Icon(Icons.search),
            prefixIconColor: Colors.grey,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.zero),
        ),
      ),
    );
  }
}
