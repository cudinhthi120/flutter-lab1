import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:lab4/views/ui/home_screen/body_gridview/ma_giam_gia.dart';
import 'package:lab4/views/ui/home_screen/top_list_view_items/department_store.dart';
import '../../../data/demo_data.dart';
import '../cart_screen/cart_screen.dart';
import '../notification_screen/notification_screen.dart';
import 'feature_category/tiki_ngon.dart';


class HomeScreen extends StatefulWidget {
  static const route = '/home';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.green,
        leading: const AppbarLeading(),
        title: const AppbarTitle(),
        actions: const [
          AppbarAction1(),
          AppbarAction2(),
        ],
        bottom: AppBar(
          backgroundColor: Colors.green,
          elevation: 0,
          title: const SearchItemField(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: const [
          TopCategories(),
          Slider(),
          GridItems(),
          FeaturedCategory(),
        ]),
      ),
    );
  }
}

class AppbarLeading extends StatelessWidget {
  const AppbarLeading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      child: Text(
        'freeship'.toUpperCase(),
        style: const TextStyle(fontSize: 10, fontWeight: FontWeight.w800),
      ),
      alignment: Alignment.centerRight,
    );
  }
}

class AppbarTitle extends StatelessWidget {
  const AppbarTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String homeScreenTitle = 'Tiki';
    return Text(homeScreenTitle);
  }
}

class AppbarAction1 extends StatelessWidget {
  const AppbarAction1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pushNamed(NotificationScreen.route);
      },
      icon: const Icon(Icons.notifications_active),
    );
  }
}

class AppbarAction2 extends StatelessWidget {
  const AppbarAction2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.of(context).pushNamed(CartScreen.route);
      },
      icon: const Icon(Icons.shopping_cart_outlined),
    );
  }
}

class SearchItemField extends StatelessWidget {
  const SearchItemField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String hintText = 'Bạn tìm gì hôm nay';

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 12),
      color: Colors.green,
      height: 60,
      child: TextField(
        cursorWidth: 1,
        cursorColor: Colors.black,
        maxLines: 1,
        textAlignVertical: TextAlignVertical.center,
        autofocus: false,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIcon: const Icon(Icons.search),
          contentPadding: EdgeInsets.zero,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(18),
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}

class TopCategories extends StatelessWidget {
  const TopCategories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 14, bottom: 5, right: 8, left: 8),
      color: Colors.green[300],
      child: SizedBox(
        height: 30,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            child: OutlinedButton(
              child: Text(
                categories[index],
                style: const TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
              style: OutlinedButton.styleFrom(
                backgroundColor: Colors.white24,
                shape: const StadiumBorder(),
                side: BorderSide(width: 0, color: Colors.white24),
              ),
              onPressed: () {
                var hintText = categories[index];
                Navigator.of(context).pushNamed(DepartmentStoreScreen.route, arguments: hintText);
              },
            ),
          ),
        ),
      ),
    );
  }
}

class Slider extends StatelessWidget {
  const Slider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
        color: Colors.green[300],
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: CarouselSlider.builder(
          itemCount: sliderImageList.length,
          itemBuilder: (context, index, pageViewIndex) => Container(
            child: Image.asset(
              sliderImageList[index],
              fit: BoxFit.cover,
            ),
          ),
          options: CarouselOptions(
            height: 270,
            viewportFraction: 1,
            autoPlayAnimationDuration: Duration(milliseconds: 500),
            initialPage: 0,
            autoPlay: true,
          ),
        ),
      ),
    );
  }
}

class GridItems extends StatelessWidget {
  const GridItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Container(
            height: 200,
            alignment: Alignment.center,
            child: GridView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: gridViewItemsImgList.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 180,
                  childAspectRatio: 1.2,
                  crossAxisSpacing: 14,
                  mainAxisSpacing: 20),
              itemBuilder: (BuildContext context, int index) => Container(
                margin: EdgeInsets.only(left: 9),
                child: GestureDetector(
                  onTap: () {
                    var discountCodeHintText = gridViewItemsName[index];
                    Navigator.of(context).pushNamed(DiscountCode.route, arguments: discountCodeHintText);
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        gridViewItemsImgList[index],
                        width: 45,
                        height: 45,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          gridViewItemsName[index],
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FeaturedCategory extends StatelessWidget {
  const FeaturedCategory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String featureCategoryContent = 'Danh mục nổi bật';
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 12, bottom: 12, top: 12),
                child: Text(
                  featureCategoryContent,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
          Container(
            alignment: Alignment.center,
            height: 300,
            child: GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: featureCategoryImgList.length,
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 180,
                  childAspectRatio: 1.4,
                  crossAxisSpacing: 14,
                  mainAxisSpacing: 8),
              itemBuilder: (BuildContext context, int index) => GestureDetector(
                onTap: () {
                  var tikiNgonHintText =  featureCategoryItemsName[index];
                  Navigator.of(context).pushNamed(TikiNgon.route, arguments: tikiNgonHintText);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      featureCategoryImgList[index],
                      width: 60,
                      height: 80,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: Text(
                        featureCategoryItemsName[index],
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
