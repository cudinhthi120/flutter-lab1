import 'package:flutter/material.dart';

class TikiNgon extends StatefulWidget {
  static const route = '/tiki-ngon';

  const TikiNgon({Key? key}) : super(key: key);

  @override
  _TikiNgonState createState() => _TikiNgonState();
}

class _TikiNgonState extends State<TikiNgon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: AppBarTextField(),
        actions: [
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.shopping_cart_outlined),
            onPressed: (){},
          ),
          IconButton(
            splashRadius: 20,
            icon: Icon(Icons.more_horiz),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}

class AppBarTextField extends StatefulWidget {
  const AppBarTextField({Key? key}) : super(key: key);

  @override
  _AppBarTextFieldState createState() => _AppBarTextFieldState();
}

class _AppBarTextFieldState extends State<AppBarTextField> {
  @override
  Widget build(BuildContext context) {
    String tikiNgonHintText = ModalRoute.of(context)!.settings.arguments as String;
    return Container(
      alignment: Alignment.center,
      height: 40,
      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        cursorHeight: 20,
        maxLines: 1,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(right: 12),
            filled: true,
            fillColor: Colors.white,
            hintText: tikiNgonHintText,
            prefixIcon: Icon(Icons.search),
            prefixIconColor: Colors.grey,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.zero),
        ),
      ),
    );
  }
}
