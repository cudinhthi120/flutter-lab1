import 'package:flutter/material.dart';
import 'new_screen_components/appbar_tabbar/community_screen/community_screen.dart';
import 'new_screen_components/appbar_tabbar/discover_screen/discover_screen.dart';
import 'new_screen_components/appbar_tabbar/follow_screen/follow_screen.dart';
import 'new_screen_components/post_screen/post_screen.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    String title = 'Lướt tin';

    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.blue,
          title: Text(title),
          bottom: PreferredSize(
            preferredSize: Size(100, 50),
            child: Container(
              width: double.infinity,
              height: 50,
              child: TabBar(
                padding: EdgeInsets.all(10),
                unselectedLabelColor: Colors.white,
                labelStyle: TextStyle(fontWeight: FontWeight.w900),
                indicator: ShapeDecoration(
                  color: Colors.white24,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                tabs: const [
                  Tab(
                    child: Text('Khám phá'),
                  ),
                  Tab(
                    child: Text('Đang theo'),
                  ),
                  Tab(
                    child: Text('Cộng đồng'),
                  )
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            DiscoverScreen(),
            FollowScreen(),
            CommunityScreen(),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).pushNamed(PostScreen.route);
          },
          child: const Icon(Icons.edit_outlined),
        ),
      ),
    );
  }
}

