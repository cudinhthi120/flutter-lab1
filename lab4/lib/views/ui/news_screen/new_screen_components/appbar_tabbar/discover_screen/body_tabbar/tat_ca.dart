import 'package:flutter/material.dart';
import '../../../../../../../data/demo_data.dart';

class TatCa extends StatefulWidget {
  const TatCa({Key? key}) : super(key: key);

  @override
  _TatCaState createState() => _TatCaState();
}

class _TatCaState extends State<TatCa> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: 4,
      itemBuilder: (context, index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            child: ListTile(
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  avatarPage1,
                  width: 60,
                ),
              ),
              title: Text(
                postsNamePage1,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(postsSubtitlePage1),
              trailing: TextButton(
                style: TextButton.styleFrom(
                    shape: StadiumBorder(),
                    backgroundColor: Colors.blue,
                    primary: Colors.white),
                onPressed: () {},
                child: Text(buttonNamePage1),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 12),
                  child: Text(postsContentPage1,
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 15)),
                ),
                ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset(postsImagePage1))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
