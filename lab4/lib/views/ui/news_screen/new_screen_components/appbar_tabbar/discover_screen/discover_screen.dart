import 'package:flutter/material.dart';
import '../../../../../../data/demo_data.dart';
import 'body_tabbar/tat_ca.dart';

class DiscoverScreen extends StatelessWidget {
  const DiscoverScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: const [
          TopListView(),
          BodyTabBar(),
        ],
      ),
    );
  }
}

class TopListView extends StatelessWidget {
  const TopListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: Container(
        color: Colors.grey[200],
        padding: EdgeInsets.only(top: 10, left: 6, bottom: 10),
        height: 60,
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: topListViewIconsList.length,
          itemBuilder: (context, index) => Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                backgroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                side: BorderSide(color: Colors.white),
              ),
              onPressed: () {
                print(topListViewTextList[index]);
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Image.asset(
                      topListViewIconsList[index],
                      width: 30,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 7),
                    child: Text(
                      topListViewTextList[index],
                      style: TextStyle(color: Colors.black87),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class BodyTabBar extends StatefulWidget {
  const BodyTabBar({Key? key}) : super(key: key);

  @override
  State<BodyTabBar> createState() => _BodyTabBarState();
}

class _BodyTabBarState extends State<BodyTabBar> {
  var tabs = [
    {"text": "Tất cả"},
    {"text": "Bỉm sữa"},
    {"text": "Cộng đồng xe"},
    {"text": "Công nghệ"},
    {"text": "Sắc đẹp"},
    {"text": "Yêu bếp Nghiện nhà"},
    {"text": "Tri thức"},
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            color: Colors.white,
            child: TabBar(
              indicatorWeight: 1,
              unselectedLabelColor: Colors.black38,
              isScrollable: true,
              labelColor: Colors.blue,
              tabs:
                tabs.map((tab) => Tab(text: tab['text'],)).toList(),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.width * 1.1,
            child: TabBarView(
              children:
                tabs.map((tab) { return TatCa();}).toList(),
            ),
          )
        ],
      ),
    );
  }
}
