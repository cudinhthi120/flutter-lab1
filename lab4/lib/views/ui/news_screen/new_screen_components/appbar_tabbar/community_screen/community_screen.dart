import 'package:flutter/material.dart';

class CommunityScreen extends StatelessWidget {
  const CommunityScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: [buildColumn(), buildColumn(), buildColumn(), buildColumn(),buildColumn(),buildColumn()],
    );
  }

  Widget buildColumn() {
    return Container(
      margin: EdgeInsets.all(8),
      child: Column(

        children: <Widget>[
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(9),
                child: Image.asset('assets/pc.png', fit: BoxFit.fill,)),
          ),
          const SizedBox(height: 10),
          Row(
            children: const [
              Expanded(
                  child: Text("Bán PC giá rẻ khoảng 200 triệu có ai mua hk? :) "),
              ),
            ],
          ),
          Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                  child: Image.asset('assets/avatar.png', width: 30,)),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Thi", maxLines: 2,),
              ),
              Spacer(),
              Icon(Icons.thumb_up),
            ],
          ),
        ],
      ),
    );
  }
}
