import 'package:flutter/material.dart';
import '../../../../../../data/demo_data.dart';

class FollowScreen extends StatelessWidget {
  const FollowScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: Column(
        children: const [
          TopText(),
          BodyListView(),
          Expanded(child: Bottom()),
        ],
      ),
    );
  }
}

class TopText extends StatelessWidget {
  const TopText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String topText = 'Nhấn Theo dõi để nhận thông tin, bài viết và hình ảnh tại chuyên mục Quan tâm';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: 100,
      width: double.infinity,
      color: Colors.white,
      child: Center(
        child: Text(
          topText,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.grey),
        ),
      ),
    );
  }
}

class BodyListView extends StatelessWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          bodyListViewTopText(),
          listView(),
        ],
      ),
    );
  }

  Widget bodyListViewTopText () {
    String title = 'Gợi ý theo dõi cho bạn';
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14, horizontal: 20),
      alignment: Alignment.centerLeft,
      child:  Text(
        title,
        style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            color: Colors.black54
        ),
      ),
    );
  }

  Widget listView () {
    return SizedBox(
      height: 130,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 6),
        scrollDirection: Axis.horizontal,
        itemCount: bodyListViewIconList.length,
        itemBuilder: (context, index) => GestureDetector(
          onTap: () {
            print(bodyListViewTitleList[index]);
          },
          child: Container(
            width: 120,
            padding: EdgeInsets.symmetric(horizontal: 8),
            margin: EdgeInsets.only(left: 14),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset(
                    bodyListViewIconList[index],
                    width: 40,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text(
                    bodyListViewTitleList[index],
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Bottom extends StatelessWidget {
  const Bottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      color: Colors.white,
    );
  }
}
