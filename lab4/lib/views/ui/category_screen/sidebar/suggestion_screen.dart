import 'package:flutter/material.dart';

class SuggestionsScreen extends StatefulWidget {
  const SuggestionsScreen({Key? key}) : super(key: key);

  @override
  _SuggestionsScreenState createState() => _SuggestionsScreenState();
}

class _SuggestionsScreenState extends State<SuggestionsScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 6),
      child: GridView.count(
          crossAxisCount: 3,
          children: [items(), items(), items(), items(),items(), items(), items(), items(), items(), items(), items(), items(), items(),items(),items(),items(),items(),items(), items(), items(), items(),],
      ),
    );
  }
}

class items extends StatelessWidget {
  const items({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(left: 4, right: 4, bottom: 6),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/pc.png', width: 70,),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Điện thoại - Máy tính bảng', maxLines: 2, overflow: TextOverflow.visible, textAlign: TextAlign.center,),
          ),
        ],
      ),
    );
  }
}

