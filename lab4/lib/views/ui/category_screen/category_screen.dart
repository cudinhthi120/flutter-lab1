import 'package:flutter/material.dart';
import '../cart_screen/cart_screen.dart';
import 'sidebar/suggestion_screen.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue,
        title: const AppbarTextField(),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(CartScreen.route);
            },
            child: Padding(
              padding: EdgeInsets.only(right: 12),
              child: Icon(Icons.shopping_cart_outlined),
            ),
          ),
        ],
      ),
      body: bodySideBar(),
    );
  }
}

class AppbarTextField extends StatefulWidget {
  const AppbarTextField({Key? key}) : super(key: key);

  @override
  _AppbarTextFieldState createState() => _AppbarTextFieldState();
}

class _AppbarTextFieldState extends State<AppbarTextField> {
  @override
  Widget build(BuildContext context) {
    String hintText =
        'Sản phẩm, thương hiệu, và mọi thứ cần thiết trong cuộc sống';

    return Container(
      height: 40,
      margin: EdgeInsets.only(right: 0),
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        maxLines: 1,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          prefixIcon: Icon(Icons.search),
          contentPadding: EdgeInsets.zero,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}

class bodySideBar extends StatefulWidget {
  const bodySideBar({Key? key}) : super(key: key);

  @override
  _bodySideBarState createState() => _bodySideBarState();
}

class _bodySideBarState extends State<bodySideBar> {
  int _selectedIndex = 0;
  List destinations = [
    {
      'label': 'Gợi ý cho bạn',
      'icon': Icons.star_border,
      'selectedIcon': Icons.star
    },
    {
      'label': 'Đồ Chơi - Mẹ\n & Bé',
      'icon': Icons.toys_outlined,
      'selectedIcon': Icons.toys
    },
    {
      'label': 'NGON',
      'icon': Icons.emoji_food_beverage_outlined,
      'selectedIcon': Icons.emoji_food_beverage
    },
    {
      'label': 'Điện thoại\n - Máy Tính Bảng',
      'icon': Icons.phone_android_outlined,
      'selectedIcon': Icons.phone_android
    },
    {
      'label': 'Làm Đẹp -\n Sức Khỏe',
      'icon': Icons.favorite_border,
      'selectedIcon': Icons.favorite
    },
    {
      'label': 'Điện Gia\n Dụng',
      'icon': Icons.electrical_services_outlined,
      'selectedIcon': Icons.electrical_services
    },
    {
      'label': 'Nhà Cửa -\n Đời Sống',
      'icon': Icons.house_outlined,
      'selectedIcon': Icons.house
    },
    {
      'label': 'Thiết bị số -\n Phụ Kiện Số',
      'icon': Icons.headset_mic_outlined,
      'selectedIcon': Icons.headset_mic
    },
    {
      'label': 'Voucher -\n Dịch Vụ',
      'icon': Icons.credit_card_outlined,
      'selectedIcon': Icons.credit_card
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SingleChildScrollView(
          child: Container(
            width: 110,
            height: 900,
            child: NavigationRail(
                backgroundColor: Colors.lightBlue[50],
                indicatorColor: Colors.blue[300],
                useIndicator: true,
                unselectedIconTheme: IconThemeData(
                  size: 35,
                  color: Colors.blue,
                ),
                selectedIconTheme: IconThemeData(
                  size: 30,
                  color: Colors.yellow[400],
                ),
                selectedLabelTextStyle: TextStyle(color: Colors.black),
                unselectedLabelTextStyle: TextStyle(
                  color: Colors.grey[500],
                ),
                selectedIndex: _selectedIndex,
                onDestinationSelected: (int index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                },
                labelType: NavigationRailLabelType.all,
                destinations: destinations
                    .map((destination) => NavigationRailDestination(
                          selectedIcon: Icon(destination['selectedIcon']),
                          icon: Icon(destination['icon']),
                          label: Text(
                            destination['label'],
                            textAlign: TextAlign.center,
                          ),
                        ))
                    .toList()),
          ),
        ),
        const VerticalDivider(thickness: 1, width: 1),
        // This is the main content
        Expanded(
          child: PageView.builder(
            itemCount: destinations.length,
              itemBuilder: (context, index) => SuggestionsScreen(),
          )
        ),
      ],
    );
  }
}
