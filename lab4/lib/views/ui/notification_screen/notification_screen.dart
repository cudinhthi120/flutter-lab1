import 'package:flutter/material.dart';

class NotificationScreen extends StatefulWidget {
  static const route = '/notification';

  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        title: Text('Thông báo'),
      ),
      body: Center(
        child: Text(
          'Hiện bạn chưa có thông báo nào',
          style: TextStyle(color: Colors.grey[700], fontSize: 18),
        ),
      ),
    );
  }
}
