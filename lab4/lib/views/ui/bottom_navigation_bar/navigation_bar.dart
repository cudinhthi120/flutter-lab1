import 'package:flutter/material.dart';
import '../home_screen/home_screen.dart';
import '../category_screen/category_screen.dart';
import '../chat_screen/chat_screen.dart';
import '../personal_screen/personal_screen.dart';
import '../news_screen/news_screen.dart';

class BotNavigationBar extends StatefulWidget {
  static const route = '/';

  const BotNavigationBar({Key? key}) : super(key: key);

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<BotNavigationBar> {
  int currentIndex = 0;
  List items = [
    {
      'label': 'Trang chủ',
      'icon': Icons.home,
    },
    {
      'label': 'Danh mục',
      'icon': Icons.category_outlined,
    },
    {
      'label': 'Lướt',
      'icon': Icons.local_fire_department_sharp,
    },
    {
      'label': 'Chat',
      'icon': Icons.chat,
    },
    {
      'label': 'Cá nhân',
      'icon': Icons.person,
    },
  ];
  final List screens = [
    const HomeScreen(),
    const CategoryScreen(),
    const NewsScreen(),
    const ChatScreen(),
    const PersonalScreen(),
  ];

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTap,
        elevation: 0,
        currentIndex: currentIndex,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        items: items
            .map((item) => BottomNavigationBarItem(
                icon: Icon(item['icon']), label: item['label']))
            .toList(),
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.black45,
      ),
    );
  }
}
