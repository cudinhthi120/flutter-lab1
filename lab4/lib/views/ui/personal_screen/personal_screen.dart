import 'package:flutter/material.dart';
import '../../../data/demo_data.dart';
import '../cart_screen/cart_screen.dart';
import '../login_and_signup/login&signup_screen.dart';
import 'list_view_items/discount_code_screen.dart';
import 'list_view_items/social_screen.dart';
import 'dart:core';

class PersonalScreen extends StatefulWidget {
  static const route = '/personal';

  const PersonalScreen({Key? key}) : super(key: key);

  @override
  _PersonalScreenState createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue,
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(CartScreen.route);
            },
            child: Padding(
              padding: EdgeInsets.only(right: 12),
              child: Icon(Icons.shopping_cart_outlined),
            ),
          )
        ],
        title: Text('Thông tin cá nhân'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            UserCard(),
            ListItems(),
          ],
        ),
      ),
    );
  }
}

class UserCard extends StatelessWidget {
  const UserCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      margin: EdgeInsets.only(bottom: 14),
      height: 70,
      child: ListTile(
        title: Text('Chào mừng bạn đến với Tiki'),
        subtitle: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(LoginAndSignUpScreen.route);
          },
          child: Text(
            'Đăng nhập/ Đăng ký',
            style: TextStyle(fontSize: 17, color: Colors.blue),
          ),
        ),
        leading: Container(
          alignment: Alignment.center,
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(40),
          ),
            child: Icon(Icons.person, color: Colors.white),
        ),
        trailing: Icon(Icons.keyboard_arrow_right_rounded),
      ),
    );
  }
}

class ListItems extends StatelessWidget {
  const ListItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(

      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: personalListViewTitle.length,
      itemBuilder: (BuildContext context, int index) => GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(personalListViewRoutes[index]);
          print(personalListViewTitle[index]);
        },
        child: Card(
          margin: EdgeInsets.only(bottom: 2),
          elevation: 0,
          child: ListTile(
            tileColor: Colors.white,
            title: Text(personalListViewTitle[index]),
            leading: Icon(personalListViewLeadingIcons[index]),
            trailing: Icon(Icons.keyboard_arrow_right_rounded),
          ),
        ),
      ),
    );
  }
}
