import 'package:flutter/material.dart';

class DiscountTabBarItems extends StatefulWidget {
  const DiscountTabBarItems({Key? key}) : super(key: key);

  @override
  _DiscountTabBarItemsState createState() => _DiscountTabBarItemsState();
}

class _DiscountTabBarItemsState extends State<DiscountTabBarItems> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 600,
          color: Colors.grey[300],
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: 8,
            itemBuilder: (context, index) => Container(
              margin: EdgeInsets.only(top: 4),
              height: 110,
              child: Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                child: Row(
                  children: [
                    Container(
                      height: double.infinity,
                      decoration: BoxDecoration(
                          border: Border(
                              right: BorderSide(width: 1, color: Colors.grey))),
                      padding: EdgeInsets.all(12),
                      child: SizedBox(
                        width: 70,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6),
                          child: Image.asset(
                            'assets/tiki-offical.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.circle_notifications_outlined,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 12),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Giảm 5K',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    'Cho đơn hàng từ 129k',
                                    style: TextStyle(
                                        color: Colors.grey[600], fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 12),
                              child: Text(
                                'HSD: 31/12/22',
                                style: TextStyle(
                                    color: Colors.grey[600], fontSize: 12),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            padding: EdgeInsets.all(12),
            color: Colors.white,
            height: 70,
            width: double.infinity,
            child: TextButton(
              style: TextButton.styleFrom(
                  primary: Colors.white,
                  backgroundColor: Colors.red,
                  textStyle: TextStyle(
                    fontSize: 16,
                  )),
              onPressed: () {},
              child: Text('Khám phá thêm mã giảm giá'),
            ),
          ),
        )
      ],
    );
  }
}
