import 'package:flutter/material.dart';

class OrderWaitingToShip extends StatefulWidget {
  static const route =   '/order_waiting_to_ship';
  const OrderWaitingToShip({Key? key}) : super(key: key);

  @override
  _OrderWaitingToShipState createState() => _OrderWaitingToShipState();
}

class _OrderWaitingToShipState extends State<OrderWaitingToShip> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đang vận chuyển'),
      ),
    );
  }
}
