import 'package:flutter/material.dart';

class TikiReceive extends StatefulWidget {
  static const route = '/tiki_receive';
  const TikiReceive({Key? key}) : super(key: key);

  @override
  _TikiReceiveState createState() => _TikiReceiveState();
}

class _TikiReceiveState extends State<TikiReceive> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đã tiếp nhận'),
      ),
    );
  }
}
