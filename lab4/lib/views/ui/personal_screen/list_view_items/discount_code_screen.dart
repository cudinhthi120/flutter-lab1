import 'package:flutter/material.dart';
import 'package:lab4/views/ui/personal_screen/list_view_items/tab_bar_items/discount_tab_bar_items.dart';
import '../../cart_screen/cart_screen.dart';

class DiscountScreen extends StatefulWidget {
  static const route = '/discount_code';

  const DiscountScreen({Key? key}) : super(key: key);

  @override
  State<DiscountScreen> createState() => _DiscountScreenState();
}

class _DiscountScreenState extends State<DiscountScreen> {
  List tabs = [
    {'text': 'Tất cả'},
    {'text': 'Tiki'},
    {'text': 'Nhà Bán'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text('Mã giảm giá'),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(CartScreen.route);
            },
            child: Padding(
              padding: EdgeInsets.only(right: 12),
              child: Icon(Icons.shopping_cart_outlined),
            ),
          ),
        ],
      ),
      body: DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Column(
          children: [
            Container(
              height: 50,
              padding: EdgeInsets.only(bottom: 6),
              color: Colors.blue,
              child: TabBar(
                labelStyle: TextStyle(fontSize: 15),
                indicatorColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicatorWeight: 4,
                tabs: tabs.map((tab) => Tab(text: tab['text'],)).toList(),
              ),
            ),
            Expanded(
              child: TabBarView(
                children: tabs.map((tab) {return DiscountTabBarItems();}).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
