import 'package:flutter/material.dart';

class SocialScreen extends StatefulWidget {
  static const route = '/social';

  const SocialScreen({Key? key}) : super(key: key);

  @override
  State<SocialScreen> createState() => _SocialScreenState();
}

class _SocialScreenState extends State<SocialScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Kết nối mạng xã hội'),
      ),
      body: BodyListView(),
    );
  }
}

class BodyListView extends StatefulWidget {
  const BodyListView({Key? key}) : super(key: key);

  @override
  State<BodyListView> createState() => _BodyListViewState();
}

class _BodyListViewState extends State<BodyListView> {
  List <bool> state = [true, false];
  List title = ['Liên kết Facebook', 'Liên kết Google Plus'];
  List icons = ['assets/facebook.png', 'assets/google-plus.png'];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: ListView.builder(
        itemCount: icons.length,
        itemBuilder: (context, index) => Card(
          elevation: 0,
          margin: EdgeInsets.only(bottom: 1),
          child: ListTile(
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(30),
                child: Image.asset(icons[index], width: 25,)),
            title: Text(title[index]),
            trailing: Switch(
              value: state[index],
              onChanged: (value) {
                setState(() {
                  state[index] = value;
                  print(state[index]);
                });
              },
              activeTrackColor: Colors.lightGreenAccent,
              activeColor: Colors.green,
            ),
          ),
        ),
      ),
    );
  }
}
