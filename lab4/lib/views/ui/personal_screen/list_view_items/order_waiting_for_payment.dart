import 'package:flutter/material.dart';

class OrderWaitingFotPayment extends StatefulWidget {
  static const route =   '/order_waiting_for_payment';
  const OrderWaitingFotPayment({Key? key}) : super(key: key);

  @override
  _OrderWaitingFotPaymentState createState() => _OrderWaitingFotPaymentState();
}

class _OrderWaitingFotPaymentState extends State<OrderWaitingFotPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đơn hàng chờ thành toán'),
      ),
    );
  }
}
