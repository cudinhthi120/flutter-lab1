import 'package:flutter/material.dart';

class OrderCanceled extends StatefulWidget {
  static const route = '/order_canceled';
  const OrderCanceled({Key? key}) : super(key: key);

  @override
  _OrderCanceledState createState() => _OrderCanceledState();
}

class _OrderCanceledState extends State<OrderCanceled> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đơn hàng bị hủy'),
      ),
    );
  }
}
