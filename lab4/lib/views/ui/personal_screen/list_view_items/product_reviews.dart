import 'package:flutter/material.dart';

class ProductReviews extends StatefulWidget {
  static const route =   '/product_reviews';
  const ProductReviews({Key? key}) : super(key: key);

  @override
  _ProductReviewsState createState() => _ProductReviewsState();
}

class _ProductReviewsState extends State<ProductReviews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đánh giá sản phẩm'),
      ),
    );
  }
}
