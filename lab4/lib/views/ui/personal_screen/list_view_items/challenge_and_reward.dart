import 'package:flutter/material.dart';

class ChallengeAndReward extends StatefulWidget {
  static const route = '/challenge_and_reward';

  const ChallengeAndReward({Key? key}) : super(key: key);

  @override
  _ChallengeAndRewardState createState() => _ChallengeAndRewardState();
}

class _ChallengeAndRewardState extends State<ChallengeAndReward> {
  List tabs = [
    {'text': 'Thử thách', 'icon': Icons.star_border},
    {'text': 'Phần thưởng', 'icon': Icons.card_giftcard},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text('Thử thách & Phần thưởng'),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: 80,
                color: Colors.blue,
              ),
              Container(
                height: 80,
                color: Colors.white,
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: DefaultTabController(
                    length: tabs.length,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 50,
                          child: TabBar(
                            indicatorColor: Colors.blue[800],
                            labelColor: Colors.blue[800],
                            unselectedLabelColor: Colors.grey[600],
                            unselectedLabelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                            tabs: tabs
                                .map((tab) => Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(tab['icon']),
                                          Text(tab['text']),
                                        ]))
                                .toList(),
                          ),
                        ),
                        Expanded(
                          child: TabBarView(
                            children: [
                              ChallengeTab(),
                              RewardTab(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 12),
            height: 150,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              elevation: 8,
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 14),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Column(
                        children: const [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              'Chào Cù Đình Thi',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                          Text(
                            'Bạn đang sở hữu',
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox(
                            height: 50,
                            child: TextButton(
                              onPressed: () {},
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '0',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.blue[800]),
                                  ),
                                  Text(
                                    'Tiki Xu',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: 0.5,
                          height: 50,
                          color: Colors.grey[400],
                          margin: EdgeInsets.only(bottom: 6),
                        ),
                        Expanded(
                          child: SizedBox(
                            height: 50,
                            child: TextButton(
                              onPressed: () {},
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '0',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.blue[800]),
                                  ),
                                  Text(
                                    'Mã giảm giá',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RewardTab extends StatefulWidget {
  const RewardTab({Key? key}) : super(key: key);

  @override
  _RewardTabState createState() => _RewardTabState();
}

class _RewardTabState extends State<RewardTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      child: Center(
        child: Text('Bạn chưa có phần thưởng nào', style: TextStyle(
          color: Colors.grey[700],
          fontSize: 16,
        ),),
      ),
    );
  }
}

class ChallengeTab extends StatefulWidget {
  const ChallengeTab({Key? key}) : super(key: key);

  @override
  _ChallengeTabState createState() => _ChallengeTabState();
}

class _ChallengeTabState extends State<ChallengeTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      child: SingleChildScrollView(
        child: Column(
          children: const [
            DealListView(),
            PlayForGiftListView(),
          ],
        ),
      ),
    );
  }
}

class DealListView extends StatefulWidget {
  const DealListView({Key? key}) : super(key: key);

  @override
  _DealListViewState createState() => _DealListViewState();
}

class _DealListViewState extends State<DealListView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 16.0, left: 12, bottom: 4),
          child: Text(
            'Săn thưởng mỗi ngày',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 180,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 2,
            itemBuilder: (context, index) => Container(
              width: 220,
              margin: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.black87,
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 130,
                    child: ClipRRect(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(12)),
                        child: Image.asset(
                          'assets/pc.png',
                          fit: BoxFit.cover,
                        )),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: const Text(
                        'Nhận xu may mắn',
                        maxLines: 1,
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class PlayForGiftListView extends StatefulWidget {
  const PlayForGiftListView({Key? key}) : super(key: key);

  @override
  _PlayForGiftListViewState createState() => _PlayForGiftListViewState();
}

class _PlayForGiftListViewState extends State<PlayForGiftListView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 16.0, left: 12, bottom: 4),
          child: Text(
            'Chơi là có quà',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 180,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 2,
            itemBuilder: (context, index) => Container(
              width: 220,
              margin: EdgeInsets.only(left: 12, top: 8, bottom: 8, right: 12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.black87,
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 130,
                    child: ClipRRect(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(12)),
                        child: Image.asset(
                          'assets/pc.png',
                          fit: BoxFit.cover,
                        )),
                  ),
                  Expanded(
                    child: Container(
                        alignment: Alignment.center,
                        child: const Text(
                          'Săn Thưởng Cùng TikiLIVE',
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                          maxLines: 1,
                          overflow: TextOverflow.visible,
                        )),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
