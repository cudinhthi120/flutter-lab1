import 'package:flutter/material.dart';

class SuccessfulOrder extends StatefulWidget {
  static const route = '/successful_order';
  const SuccessfulOrder({Key? key}) : super(key: key);

  @override
  _SuccessfulOrderState createState() => _SuccessfulOrderState();
}

class _SuccessfulOrderState extends State<SuccessfulOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Thành công'),
      ),
    );
  }
}
