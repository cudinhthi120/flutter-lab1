import 'package:flutter/material.dart';
import 'views/ui/home_screen/body_gridview/ma_giam_gia.dart';
import 'views/ui/home_screen/feature_category/tiki_ngon.dart';
import 'views/ui/home_screen/top_list_view_items/department_store.dart';
import 'views/ui/chat_screen/box_chat_screen.dart';
import 'views/ui/news_screen/new_screen_components/post_screen/post_screen.dart';
import 'views/ui/personal_screen/list_view_items/order_management.dart';
import 'views/ui/personal_screen/list_view_items/order_waiting_for_payment.dart';
import 'views/ui/personal_screen/list_view_items/order_waiting_to_ship.dart';
import 'views/ui/personal_screen/list_view_items/product_reviews.dart';
import 'views/ui/personal_screen/list_view_items/successful_order.dart';
import 'views/ui/personal_screen/list_view_items/tiki_receive.dart';
import 'views/ui/personal_screen/list_view_items/order_has_been_canceled.dart';
import 'views/ui/personal_screen/list_view_items/challenge_and_reward.dart';
import 'views/ui/notification_screen/notification_screen.dart';
import 'views/ui/cart_screen/cart_screen.dart';
import 'views/ui/chat_screen/chat_screen.dart';
import 'views/ui/home_screen/home_screen.dart';
import 'views/ui/personal_screen/list_view_items/discount_code_screen.dart';
import 'views/ui/personal_screen/list_view_items/social_screen.dart';
import 'views/ui/login_and_signup/login&signup_screen.dart';
import 'views/ui/bottom_navigation_bar/navigation_bar.dart';
import 'views/ui/personal_screen/personal_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App-tiki-clone',
      routes: {
        '/': (context) => BotNavigationBar(),
        '/home': (context) => HomeScreen(),
        '/department-store': (context) => DepartmentStoreScreen(),
        '/discount-code': (context) => DiscountCode(),
        '/tiki-ngon': (context) => TikiNgon(),
        '/chat': (context) => ChatScreen(),
        '/box-chat': (context) => BoxChatScreen(),
        '/cart': (context) => CartScreen(),
        '/post': (context) => PostScreen(),
        '/notification': (context) => NotificationScreen(),
        '/login': (context) => LoginAndSignUpScreen(),
        '/personal': (context) => PersonalScreen(),
        // personal list view items
        '/social': (context) => SocialScreen(),
        '/discount_code': (context) => DiscountScreen(),
        '/challenge_and_reward': (context) => ChallengeAndReward(),
        '/order_canceled': (context) => OrderCanceled(),
        '/order_management': (context) => OrderManagement(),
        '/order_waiting_for_payment': (context) => OrderWaitingFotPayment(),
        '/order_waiting_to_ship': (context) => OrderWaitingToShip(),
        '/product_reviews': (context) => ProductReviews(),
        '/successful_order': (context) => SuccessfulOrder(),
        '/tiki_receive' : (context) => TikiReceive(),

      },
      initialRoute: '/',
    );
  }
}

