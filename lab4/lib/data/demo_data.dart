// HomeScreen data
import 'package:flutter/material.dart';

List sliderImageList = [
  'assets/pc.png',
  'assets/pc2.png',
  'assets/laptop.png',
  'assets/interior.png',
  'assets/clothes.png',
];

List<String> categories = [
  'Thit, Rau Củ',
  'Bách Hóa',
  'Nhà Cửa',
  'Điện tử',
  'Thiết bị số',
  'Điện thoại'
];

List gridViewItemsImgList = [
  'assets/ma-giam-gia.png',
  'assets/shop-theo-nganh.png',
  'assets/uu-dai-thanh-toan.png',
  'assets/dich-vu-tien-ich.png',
  'assets/san-thương-moi-ngay.png',
  'assets/di-cho-online.png',
  'assets/giam-gia-50.png',
  'assets/khong-gioi-han.png',
  'assets/binh-on-gia.png',
  'assets/danh-gia-san-pham.png',
  'assets/hoang-tien.png',
  'assets/goi-hoi-vien.png',
];

List gridViewItemsName = [
  'Mã giảm giá',
  'Shop theo ngành hàng',
  'Ưu đãi thanh toán',
  'Dịch vụ & Tiện ích',
  'Săn thưởng mỗi ngày',
  'Đi chợ online',
  'Giảm giá đến 50%',
  'Không giới hạn',
  'Bình ổn giá',
  'Đánh giá sản phầm',
  'Hoàng tiền 15%',
  'Gói hội viên',
];

List featureCategoryImgList = [
  'assets/tiki-ngon.png',
  'assets/giay-the-thao-nam.png',
  'assets/balo.png',
  'assets/sach-tu-duy.png',
  'assets/smartphone.png',
  'assets/truyen-tranh.png',
  'assets/tieu-thuyet.png',
  'assets/truyen-ngan.png',
  'assets/ao-thun-nam.png',
  'assets/ban-ghe-lam-viec.png',
  'assets/tu.png',
  'assets/binh-giu-nhiet.png',
];

List featureCategoryItemsName = [
  'TikiNGON',
  'Giày thể thao nam',
  'Balo',
  'Sách tư duy - Kỹ năng sống',
  'Điện thoại SmartPhone',
  'Truyện Tranh, Manga, Comic',
  'Tiểu Thuyết',
  'Truyện ngắn',
  'Áo thun nam',
  'Bàn ghế làm việc',
  'Tủ',
  'Bình giữ nhiệt',
];
// End HomeScreen data

// ChatScreen data
List imgList = ['assets/laptop.png', 'assets/pc2.png', 'assets/smartphone.png',];
List titleList = ['Shop Laptop', 'Shop PC', 'Shop Điện thoại'];
List subTitleList = ['Shop Hào Computer', 'Shop Vũ PC', 'Shop Luke Iphone'];
// End ChatScreen data

// BoxChat data
class ChatMessage {
  String messageContent;
  String messageType;

  ChatMessage({required this.messageContent, required this.messageType});
}
List<ChatMessage> messages = [
  ChatMessage(messageContent: "Chào shop", messageType: "sender"),
  ChatMessage(messageContent: "Chào bạn, không biết mình có thể giúp gì cho bạn", messageType: "receiver"),
  ChatMessage(messageContent: "Không biết bên shop bạn có bạn bàn phím giả cơ không nhỉ?", messageType: "sender"),
  ChatMessage(messageContent: "Dạ bên mình có bạn", messageType: "receiver"),
];
// End BoxChat Data

// NewsScreen data
  // DiscoverScreen data
  List topListViewIconsList = [
    'assets/tiki-live.png',
    'assets/giai-tri.png',
    'assets/mua-sam.png',
    'assets/xu-huong.png',
    'assets/nhan-qua.png',
    'assets/coupon.png',
    'assets/nhin-lai-2021.png'
  ];

  List topListViewTextList = [
    'Tiki Live',
    'Giải trí',
    'Mua sắm',
    '#Tikixuhuong',
    'Mở thẻ - Nhận quà',
    ' Nhận ngay Coupon',
    'Nhìn lại 2021'
  ];

    // TatCaScreen Data
    String avatarPage1 = 'assets/cat.png';
    String postsContentPage1 = 'Bộ PC 200 củ không biết có ai mua không ta';
    String buttonNamePage1 = 'Theo dõi';
    String postsImagePage1 = 'assets/pc.png';
    String postsNamePage1 = 'Bài 1';
    String postsSubtitlePage1 = 'Bán PC';
    //End TatCaScreen Data

    // BimSuaXeScreen Data
    String avatarPage2 = 'assets/cat.png';
    String postsContentPage2 = 'Mua sữa liền tay -  Nhận ngay Voucher';
    String buttonNamePage2 = 'Theo dõi';
    String postsImagePage2 = 'assets/bim-sua.png';
    String postsNamePage2 = 'Bài 1';
    String postsSubtitlePage2 = 'Bán sữa';
    // End BimSuaScreen Data

    // CongDongXeScreen Data
    String avatarPage3 = 'assets/cat.png';
    String postsContentPage3 = 'Xe mới 99% bán lại giá rẻ';
    String buttonNamePage3 = 'Theo dõi';
    String postsImagePage3 = 'assets/cong-dong-xe.png';
    String postsNamePage3 = 'Bài 1';
    String postsSubtitlePage3 = 'Bán xe';
    // End CongDongXeScreen Data

    // CongNgheScreen Data
    String avatarPage4 = 'assets/cat.png';
    String postsContentPage4 = 'Máy đo nhiệt độ 5 trong 1';
    String buttonNamePage4 = 'Theo dõi';
    String postsImagePage4 = 'assets/cong-nghe.png';
    String postsNamePage4 = 'Bài 1';
    String postsSubtitlePage4 = 'Bán đồ công nghệ';
    // End CongNgheScreen Data

    // SacDepScreen Data
    String avatarPage5 = 'assets/cat.png';
    String postsContentPage5 = 'Kem dưỡng mắt tốt nhất 2022';
    String buttonNamePage5 = 'Theo dõi';
    String postsImagePage5 = 'assets/sac-dep.png';
    String postsNamePage5 = 'Bài 1';
    String postsSubtitlePage5 = 'Bán mỹ phẩm';
    // End SacDepScreen Data

    // YeuBepNghienNhaScreen Data
    String avatarPage6 = 'assets/cat.png';
    String postsContentPage6 = 'Bộ nồi Cavalli dùng cho mọi loại bếp';
    String buttonNamePage6 = 'Theo dõi';
    String postsImagePage6 = 'assets/yeu-bep-nghien-nha.png';
    String postsNamePage6 = 'Bài 1';
    String postsSubtitlePage6 = 'Bán đồ bếp';
    // End YeuBepNghienNhaScreen Data

    // TriThucScreen Data
    String avatarPage7 = 'assets/cat.png';
    String postsContentPage7 = 'Kỹ năng thuyết trình trước đám đông - Làm thế nào để tự tin Thuyết trình trước đám đông';
    String buttonNamePage7 = 'Theo dõi';
    String postsImagePage7 = 'assets/tri-thuc.png';
    String postsNamePage7 = 'Bài 1';
    String postsSubtitlePage7 = 'Bán PC';
    // End TriThucScreen Data

  // End DiscoverScreen data

  // FollowScreen data
  List bodyListViewIconList = [
    'assets/tien-ich.png',
    'assets/ticket-box.png',
    'assets/tiki-trading.png',
    'assets/tiki-offical.png',
    'assets/banuli.png',
  ];

  List bodyListViewTitleList = [
    'Kho Tiện Ích Offical',
    'Ticketbox',
    'Tiki Trading',
    'Tiki Offical',
    'BANULI',
  ];
  // End FollowScreen Data
// End NewsScreen Data

// PersonalScreen Data
List personalListViewTitle = <String>[
  'Kết nối mạng xã hội',
  'Mã giảm giá',
  'Săn thưởng',
  'Đánh giá sản phẩm',
  'Quản lý đơn hàng',
  'Tiki tiếp nhận',
  'Đơn hàng chờ thanh toán',
  'Đơn hàng đang chờ vận chuyển',
  'ĐƠn hàng thành công',
  'Đơn hàng đã hủy',
  'Cài Đặt',
];

List personalListViewLeadingIcons = [
  Icons.connect_without_contact,
  Icons.card_membership_outlined,
  Icons.card_giftcard,
  Icons.star_rate,
  Icons.text_snippet,
  Icons.add_task_outlined,
  Icons.payment_sharp,
  Icons.delivery_dining,
  Icons.check,
  Icons.disabled_by_default,
  Icons.settings,
];

List personalListViewRoutes = [
  '/social',
  '/discount_code',
  '/challenge_and_reward',
  '/product_reviews',
  '/order_management',
  '/tiki_receive',
  '/order_waiting_for_payment',
  '/order_waiting_to_ship',
  '/successful_order',
  '/order_canceled',
];
// End PersonalScreen Data